# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import Source
from test.models.test_models import TestModels
from test.models.test_service import TestServiceModel


log = logging.getLogger(__name__)


class TestSourceModel(TestModels):
    @staticmethod
    def get_test_source(is_per_user=False):
        service = TestServiceModel.get_test_service()
        service.save()

        return Source(
            service=service,
            script_name='%sWorker' % random_string(),
            name='Source %s' % random_string(),
            description='Test Source Description',
            is_per_user=is_per_user
        )

    def test_source(self):
        s = TestSourceModel.get_test_source()
        s.save()

        r = Source.objects.get(pk=s.id)

        self.assertEqual(r.service, s.service)
        self.assertEqual(r.script_name, s.script_name)
        self.assertEqual(r.name, s.name)
        self.assertEqual(r.description, s.description)
        self.assertFalse(r.is_per_user)
        self.assertEqual(r.min_refresh, s.min_refresh)
        self.assertEqual(r.is_active, s.is_active)