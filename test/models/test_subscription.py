# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta

from common.enums import SubscriptionStatus
from common.helpers import now
from robot.models import Subscription
from robot.models import UserSubscription
from test.models.test_models import TestModels
from test.models.test_service_profile import TestServiceProfileModel
from test.models.test_source import TestSourceModel
from test.models.test_user import TestUserModel

log = logging.getLogger(__name__)


class TestSubscriptionModel(TestModels):
    @staticmethod
    def get_test_subscription(value=None):
        source = TestSourceModel.get_test_source()
        source.save()

        user = TestUserModel.get_test_user()
        user.save()

        profile = TestServiceProfileModel.get_test_service_profile()
        profile.save()

        return Subscription(
            source=source,
            user=user,
            profile=profile,
            value=value
        )

    @staticmethod
    def get_test_subscription_with_subscribers(how_many=1):
        s = TestSubscriptionModel.get_test_subscription()
        s.save()

        for i in xrange(0, how_many):
            user = TestUserModel.get_test_user()
            user.save()

            profile = TestServiceProfileModel.get_test_service_profile()
            profile.save()

            us = UserSubscription(
                subscription=s,
                user=user,
                profile=profile
            )
            us.save()

        return s

    @staticmethod
    def get_test_subscription_with_value(value):
        return TestSubscriptionModel.get_test_subscription(value=value)

    def test_subscription(self):
        t = TestSubscriptionModel.get_test_subscription()
        t.save()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(t.source, r.source)
        self.assertEqual(t.profile, r.profile)
        self.assertIsNone(r.value)
        self.assertIsNone(r.last_refresh_at)
        self.assertTrue(r.due_refresh)
        self.assertEqual(r.status, str(SubscriptionStatus.READY))
        self.assertEqual(r.tokens_used, 0)
        self.assertEqual(r.tokens_left, 0)
        self.assertIsNone(r.tokens_reset_at)
        self.assertTrue(r.is_active)
        self.assertEqual(t.created_at, r.created_at)
        self.assertEqual(t.updated_at, r.updated_at)
        self.assertIsInstance(r.created_at, datetime)

    def test_subscription_ready(self):
        t = TestSubscriptionModel.get_test_subscription()
        t.status = str(SubscriptionStatus.FAILED)
        t.save()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.FAILED))

        r.set_as_ready()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.READY))

    def test_subscription_enqueued(self):
        t = TestSubscriptionModel.get_test_subscription()
        t.save()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.READY))
        self.assertTrue(r.due_refresh)

        r.set_as_enqueued()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.ENQUEUED))
        self.assertFalse(r.due_refresh)

    def test_subscription_in_progress(self):
        t = TestSubscriptionModel.get_test_subscription()
        t.save()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.READY))
        self.assertTrue(r.due_refresh)

        r.set_as_in_progress()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.IN_PROGRESS))
        self.assertFalse(r.due_refresh)

    def test_subscription_failed(self):
        t = TestSubscriptionModel.get_test_subscription()
        t.save()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.READY))
        self.assertTrue(r.due_refresh)

        r.set_as_failed()

        r = Subscription.objects.get(pk=t.id)

        self.assertEqual(r.status, str(SubscriptionStatus.FAILED))
        self.assertTrue(r.due_refresh)

    def test_find_and_update_ready_to_refresh(self):
        pass

    def test_next_to_run(self):
        Subscription.objects.all().delete()

        t = TestSubscriptionModel.get_test_subscription_with_subscribers()
        t.save()

        t = TestSubscriptionModel.get_test_subscription_with_subscribers()
        t.due_refresh = False
        t.save()

        t = TestSubscriptionModel.get_test_subscription_with_subscribers()
        t.save()

        tasks = Subscription.objects.next_to_run()

        self.assertEqual(len(tasks), 2)

        self.assertEqual(tasks[0].due_refresh, False)
        self.assertEqual(tasks[0].status, str(SubscriptionStatus.ENQUEUED))
        self.assertEqual(tasks[1].due_refresh, False)
        self.assertEqual(tasks[1].status, str(SubscriptionStatus.ENQUEUED))

        subs = Subscription.objects.filter(status=str(SubscriptionStatus.READY))

        self.assertEqual(len(subs), 1)
        self.assertEqual(tasks[0].due_refresh, False)

    def test_has_subscribers(self):
        t = TestSubscriptionModel.get_test_subscription_with_subscribers()
        t.save()

        self.assertTrue(t.has_subscribers())

        t = TestSubscriptionModel.get_test_subscription()
        t.save()

        self.assertFalse(t.has_subscribers())

    def test_no_tokens_left(self):
        s = TestSubscriptionModel.get_test_subscription()
        s.tokens_used = 5000
        s.tokens_left = 1000
        s.tokens_reset_at = None
        s.save()

        r = Subscription.objects.get(pk=s.id)

        self.assertEqual(r.tokens_used, 5000)
        self.assertEqual(r.tokens_left, 1000)
        self.assertFalse(r.no_tokens_left())

        s.tokens_left = 1000
        s.tokens_reset_at = now() + timedelta(seconds=60)
        self.assertFalse(s.no_tokens_left())

        s.tokens_left = 0
        self.assertTrue(s.no_tokens_left())

        s.tokens_left = 1000
        s.tokens_reset_at = now() - timedelta(seconds=7200)
        self.assertFalse(s.no_tokens_left())

        s.tokens_left = 0
        s.tokens_reset_at = now() + timedelta(seconds=7200)
        self.assertTrue(s.no_tokens_left())

    def test_subscribers(self):
        t = TestSubscriptionModel.get_test_subscription_with_subscribers(3)
        t.save()

        self.assertEqual(len(t.subscribers()), 3)

    def test_add_subscriber(self):
        s = TestSubscriptionModel.get_test_subscription()
        s.save()

        u = TestUserModel.get_test_user()
        u.save()

        p = TestServiceProfileModel.get_test_service_profile()
        p.save()

        self.assertEqual(len(s.subscribers()), 0)

        s.add_subscriber(u, p)

        self.assertEqual(len(s.subscribers()), 1)

        s.remove_subscriber(u, p)

        self.assertEqual(len(s.subscribers()), 0)

    def test_add_subscription(self):
        s = TestSourceModel.get_test_source()
        s.save()

        p = TestServiceProfileModel.get_test_service_profile()
        p.save()

        u = TestUserModel.get_test_user()
        u.save()

        sub = Subscription.add_subscription(source=s, user=u, profile=p)

        self.assertIsNotNone(sub.id)
        self.assertEqual(sub.source, s)
        self.assertEqual(sub.user, u)
        self.assertEqual(sub.profile, p)
        self.assertIsNone(sub.value)

        sub = Subscription.add_subscription(source=s, user=u, profile=p, value='value')

        self.assertIsNotNone(sub.id)
        self.assertEqual(sub.source, s)
        self.assertEqual(sub.user, u)
        self.assertEqual(sub.profile, p)
        self.assertIsNotNone(sub.value)