# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from robot.models import UserSubscription
from test.models.test_models import TestModels
from test.models.test_service_profile import TestServiceProfileModel
from test.models.test_subscription import TestSubscriptionModel
from test.models.test_user import TestUserModel

log = logging.getLogger(__name__)


class TestUserSubscriptionModel(TestModels):
    @staticmethod
    def get_test_user_subscription():
        sub = TestSubscriptionModel.get_test_subscription()
        sub.save()

        user = TestUserModel.get_test_user()
        user.save()

        profile = TestServiceProfileModel.get_test_service_profile()
        profile.save()

        return UserSubscription(
            subscription=sub,
            user=user,
            profile=profile
        )

    def test_user_subscription(self):
        t = TestUserSubscriptionModel.get_test_user_subscription()
        t.save()

        r = UserSubscription.objects.get(pk=t.id)

        self.assertEqual(t.subscription, r.subscription)
        self.assertEqual(t.profile, r.profile)
        self.assertEqual(t.created_at, r.created_at)
        self.assertEqual(t.updated_at, r.updated_at)
        self.assertIsInstance(r.created_at, datetime)