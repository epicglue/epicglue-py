# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.models import User

from common.helpers import random_string
from common.models import UserProfile
from test.models.test_models import TestModels


log = logging.getLogger(__name__)


class TestUserModel(TestModels):
    @staticmethod
    def get_test_user():
        name = random_string()

        return User.objects.create_user(name, '%s@example.com' % name, '1234')

    @staticmethod
    def get_test_user_profile():
        user = TestUserModel.get_test_user()
        user.save()

        return UserProfile(
            user=user,
            username=user.username
        )

    def test_user(self):
        u = TestUserModel.get_test_user()
        u.save()

        r = User.objects.get(pk=u.id)

        self.assertEqual(r.email, u.email)
        self.assertEqual(r.username, u.username)
        self.assertEqual(r.password, u.password)

    def test_user_profile(self):
        u = TestUserModel.get_test_user_profile()
        u.save()

        r = UserProfile.objects.get(pk=u.id)

        self.assertEqual(r.username, u.username)
        self.assertEqual(r.plan, u.plan)