# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import Service
from test.models.test_category import TestCategoryModel
from test.models.test_models import TestModels


log = logging.getLogger(__name__)


class TestServiceModel(TestModels):
    @staticmethod
    def get_test_service():
        cat = TestCategoryModel.get_test_category()
        cat.save()

        return Service(
            category=cat,
            name='Service %s' % random_string(),
            short_name='service_%s' % random_string(),
            description='Test Service Description'
        )

    def test_service(self):
        s = TestServiceModel.get_test_service()
        s.save()

        r = Service.objects.get(name=s.name)

        self.assertEqual(str(s), s.name)
        self.assertEqual(r.category, s.category)
        self.assertEqual(r.name, s.name)
        self.assertEqual(r.short_name, s.short_name)
        self.assertEqual(r.description, s.description)
        self.assertEqual(r.is_visible, s.is_visible)