# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import UserServiceProfile
from test.models.test_models import TestModels
from test.models.test_service import TestServiceModel
from test.models.test_user import TestUserModel


log = logging.getLogger(__name__)


class TestServiceProfileModel(TestModels):
    @staticmethod
    def get_test_service_profile():
        s = TestServiceModel.get_test_service()
        s.save()

        u = TestUserModel.get_test_user()
        u.save()

        return UserServiceProfile(
            user=u,
            service=s,
            identifier=random_string(30),
            friendly_name='User %s' % random_string(8)
        )

    @staticmethod
    def get_test_service_profile_for_username(username):
        sp = TestServiceProfileModel.get_test_service_profile()
        sp.identifier = username

        return sp

    def test_service(self):
        s = TestServiceProfileModel.get_test_service_profile()
        s.save()

        r = UserServiceProfile.objects.get(id=s.id)

        self.assertEqual(r.user, s.user)
        self.assertEqual(r.service, s.service)
        self.assertEqual(r.identifier, s.identifier)
        self.assertEqual(r.friendly_name, s.friendly_name)