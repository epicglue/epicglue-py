# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import SubscriptionLog
from test.models.test_models import TestModels
from test.models.test_subscription import TestSubscriptionModel


log = logging.getLogger(__name__)


class TestSubscriptionLogModel(TestModels):
    @staticmethod
    def get_test_subscription_log(code=200, msg=None):
        sub = TestSubscriptionModel.get_test_subscription()
        sub.save()

        return SubscriptionLog(
            subscription=sub,
            code=code,
            msg=msg
        )

    def test_add_log(self):
        sl = TestSubscriptionLogModel.get_test_subscription_log()
        sl.save()

        r = SubscriptionLog.objects.get(pk=sl.id)

        self.assertEqual(r.subscription, sl.subscription)
        self.assertEqual(r.url, sl.url)
        self.assertEqual(r.code, sl.code)
        self.assertIsNone(sl.msg)

    def test_add_log_with_msg(self):
        sl = TestSubscriptionLogModel.get_test_subscription_log(msg=random_string(500))
        sl.save()

        r = SubscriptionLog.objects.get(pk=sl.id)

        self.assertEqual(r.subscription, sl.subscription)
        self.assertEqual(r.url, sl.url)
        self.assertEqual(r.code, sl.code)
        self.assertEqual(r.msg, sl.msg)