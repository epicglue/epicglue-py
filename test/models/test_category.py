# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import Category
from test.models.test_models import TestModels


log = logging.getLogger(__name__)


class TestCategoryModel(TestModels):
    @staticmethod
    def get_test_category():
        return Category(
            upper_category=None,
            name='Cat %s' % random_string(),
            description='Test Category Description'
        )

    @staticmethod
    def get_test_multi_level_category():
        c1 = TestCategoryModel.get_test_category()
        c1.save()

        c2 = TestCategoryModel.get_test_category()
        c2.upper_category = c1

        return c2

    def test_category(self):
        c = TestCategoryModel.get_test_category()
        c.save()

    def test_multi_level_category(self):
        c = TestCategoryModel.get_test_multi_level_category()
        c.save()

        r = Category.objects.get(name=c.name)

        self.assertEqual(str(c), c.name)
        self.assertEqual(r.upper_category, c.upper_category)
        self.assertEqual(r.name, c.name)
        self.assertEqual(r.description, c.description)
        self.assertEqual(r.is_visible, c.is_visible)