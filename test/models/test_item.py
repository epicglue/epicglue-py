# -*- coding: utf-8 -*-
import logging

from robot.models import Item
from common.enums import MediaType, Visibility
from common.enums import ItemType
from common.helpers import random_string, now
from test.models.test_models import TestModels
from test.models.test_service import TestServiceModel


log = logging.getLogger(__name__)


class TestItemModel(TestModels):
    @staticmethod
    def get_test_item():
        service = TestServiceModel.get_test_service()
        service.save()

        return Item(
            content_id=random_string(64),
            content_created_at=now(),
            content_order_by_date=now(),
            item_type=str(ItemType.HACKER_NEWS),
            media_type=str(MediaType.LINK),
            service=service.name,
            title='Title: %s' % random_string(20),
            description='Description: %s' % random_string(200),
            author=random_string(),
            tags=[random_string(8), random_string(8), random_string(8)],
            visibility=str(Visibility.PUBLIC),
            location=[34.711863329, -118.063882934],
            points=100,
            comments=10,
            image='http://www.example.com/image.png',
            image_width=300,
            image_height=300,
            image_large='http://www.example.com/image.png',
            image_large_width=500,
            image_large_height=500,
            image_small='http://www.example.com/image.png',
            image_small_width=150,
            image_small_height=150,
            url='http://www.example.com',
            created_at=now()
        )

    def test_hash(self):
        t = TestItemModel.get_test_item()

        self.assertEqual(t.hash, '')
        t.save()
        self.assertIsNotNone(t.hash)

        r = Item.objects.get(pk=t.hash)

        self.assertEqual(t.hash, r.hash)

    def test_item(self):
        t = TestItemModel.get_test_item()
        t.save()

        r = Item.objects.get(pk=t.hash)

        self.assertEqual(t.hash, r.hash)
        self.assertEqual(t.content_id, r.content_id)
        self.assertEqual(t.content_created_at, r.content_created_at)
        self.assertEqual(t.content_updated_at, r.content_updated_at)
        self.assertEqual(t.item_type, r.item_type)
        self.assertEqual(t.media_type, r.media_type)
        self.assertEqual(t.service, r.service)
        self.assertEqual(t.title, r.title)
        self.assertEqual(t.description, r.description)
        self.assertEqual(t.image, r.image)
        self.assertEqual(t.image_width, r.image_width)
        self.assertEqual(t.image_height, r.image_height)
        self.assertEqual(t.image_large, r.image_large)
        self.assertEqual(t.image_large_width, r.image_large_width)
        self.assertEqual(t.image_large_height, r.image_large_height)
        self.assertEqual(t.image_small, r.image_small)
        self.assertEqual(t.image_small_width, r.image_small_width)
        self.assertEqual(t.image_small_height, r.image_small_height)
        self.assertEqual(t.tags, r.tags)
        self.assertListEqual(t.location, [34.711863329, -118.063882934])
        self.assertEqual(t.url, r.url)
        self.assertEqual(t.visibility, r.visibility)
        self.assertEqual(t.points, r.points)
        self.assertEqual(t.comments, r.comments)
        self.assertEqual(t.created_at, r.created_at)
        self.assertEqual(t.updated_at, r.updated_at)

    def test_to_json(self):
        item = TestItemModel.get_test_item()
        item.save()

        r = Item.objects.get(pk=item.hash)

        json = r.to_json(False)

        self.assertEqual(r.content_id, json['content_id'])
        self.assertEqual(r.content_created_at, json['content_created_at'])
        self.assertEqual(r.item_type, json['item_type'])
        self.assertEqual(r.media_type, json['media_type'])
        self.assertEqual(r.service, json['service'])
        self.assertEqual(r.author, json['author'])
        self.assertEqual(r.visibility, json['visibility'])
        self.assertEqual(len(json.keys()), 23)

    def test_from_json(self):
        service = TestServiceModel.get_test_service()
        service.save()

        json = {
            'content_id': random_string(64),
            'content_created_at': now(),
            'content_order_by_date': now(),
            'item_type': str(ItemType.HACKER_NEWS),
            'media_type': str(MediaType.LINK),
            'service': service.name,
            'title': 'Title: %s' % random_string(20),
            'description': 'Description: %s' % random_string(200),
            'author': random_string(),
            'visibility': str(Visibility.PUBLIC),
            'image': 'http://www.example.com/image.png',
            'image_width': 300,
            'image_height': 300,
            'image_large': 'http://www.example.com/image.png',
            'image_large_width': 500,
            'image_large_height': 500,
            'image_small': 'http://www.example.com/image.png',
            'image_small_width': 150,
            'image_small_height': 150,
            'url': 'http://www.example.com',
            'points': 100,
            'comments': 10
        }

        item = Item.from_json(json)
        item.save()

        log.info(item.to_es_json())
        log.info(item.pk)

        r = Item.objects.get(pk=item.hash)

        self.assertEqual(r.content_id, json['content_id'])
        self.assertEqual(r.content_created_at, json['content_created_at'])
        self.assertEqual(r.item_type, json['item_type'])
        self.assertEqual(r.media_type, json['media_type'])
        self.assertEqual(r.service, json['service'])
        self.assertEqual(r.author, json['author'])
        self.assertEqual(r.visibility, json['visibility'])
        self.assertEqual(r.image, json['image'])
        self.assertEqual(r.image_width, json['image_width'])
        self.assertEqual(r.image_height, json['image_height'])
        self.assertEqual(r.image_small, json['image_small'])
        self.assertEqual(r.image_small_width, json['image_small_width'])
        self.assertEqual(r.image_small_height, json['image_small_height'])
        self.assertEqual(r.image_large, json['image_large'])
        self.assertEqual(r.image_large_width, json['image_large_width'])
        self.assertEqual(r.image_large_height, json['image_large_height'])
        self.assertEqual(r.url, json['url'])
        self.assertEqual(r.points, json['points'])
        self.assertEqual(r.comments, json['comments'])

    def test_to_es_json(self):
        item = TestItemModel.get_test_item()
        item.save()

        r = Item.objects.get(pk=item.hash)

        json = r.to_es_json()

        self.assertEqual(r.title, json['title'])
        self.assertEqual(r.description, json['description'])
        self.assertEqual(r.image, json['image'])
        self.assertEqual(r.image_width, json['image_width'])
        self.assertEqual(r.image_height, json['image_height'])
        self.assertEqual(r.image_small, json['image_small'])
        self.assertEqual(r.image_small_width, json['image_small_width'])
        self.assertEqual(r.image_small_height, json['image_small_height'])
        self.assertEqual(r.image_large, json['image_large'])
        self.assertEqual(r.image_large_width, json['image_large_width'])
        self.assertEqual(r.image_large_height, json['image_large_height'])
        self.assertEqual(r.url, json['url'])
        self.assertEqual(r.points, json['points'])
        self.assertEqual(r.comments, json['comments'])
        self.assertEqual(r.location[0], json['location']['lat'])
        self.assertEqual(r.location[1], json['location']['lon'])
        self.assertEqual(len(json.keys()), 25)

    def test_to_es_update_json(self):
        item = TestItemModel.get_test_item()
        item.save()

        r = Item.objects.get(pk=item.hash)

        json = r.to_es_update_json()

        self.assertEqual(r.points, json['points'])
        self.assertEqual(r.comments, json['comments'])
        self.assertEqual(len(json.keys()), 3)