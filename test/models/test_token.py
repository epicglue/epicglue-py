# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string, now
from robot.models import UserServiceToken
from test.models.test_models import TestModels
from test.models.test_service_profile import TestServiceProfileModel


log = logging.getLogger(__name__)


class TestTokenModel(TestModels):
    @staticmethod
    def get_test_token():
        p = TestServiceProfileModel.get_test_service_profile()
        p.save()

        return UserServiceToken(
            profile=p,
            token=random_string(255),
            token_secret=random_string(255),
            refresh_token=random_string(255),
            expiry=now(),
        )

    def test_token(self):
        t = TestTokenModel.get_test_token()
        t.save()

        r = UserServiceToken.objects.get(pk=t.id)

        self.assertEqual(r.profile, t.profile)
        self.assertEqual(r.token, t.token)
        self.assertEqual(r.token_secret, t.token_secret)
        self.assertEqual(r.refresh_token, t.refresh_token)
        self.assertEqual(r.expiry, t.expiry)
        self.assertTrue(r.is_active)