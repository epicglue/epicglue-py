# -*- coding: utf-8 -*-
import logging

from common.helpers import random_string
from robot.models import State
from test.models.test_models import TestModels
from test.models.test_subscription import TestSubscriptionModel


log = logging.getLogger(__name__)


class TestStateModel(TestModels):
    @staticmethod
    def get_test_state():
        sub = TestSubscriptionModel.get_test_subscription()
        sub.save()

        return State(
            subscription=sub,
            value=random_string(255)
        )

    def test_state(self):
        s = TestStateModel.get_test_state()
        s.save()

        r = State.objects.get(pk=s.id)

        self.assertEqual(r.subscription, s.subscription)
        self.assertEqual(r.value, s.value)

    def test_for_subscription(self):
        s = TestStateModel.get_test_state()
        s.save()

        state = State.for_subscription(s.subscription)

        self.assertIsInstance(state, State)
        self.assertEqual(state.id, s.id)
        self.assertEqual(state.value, s.value)