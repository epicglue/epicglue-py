# -*- coding: utf-8 -*-
import logging
import unittest

from common.enums import Service as ServiceEnum
from robot.models import Service
from test.models.test_category import TestCategoryModel
from test.robot.worker.test_worker import WorkerTestCase

log = logging.getLogger(__name__)


class RedditWorkerTestCase(WorkerTestCase):
    def get_service(self):
        try:
            return Service.objects.get(short_name=str(ServiceEnum.REDDIT))
        except Service.DoesNotExist:
            cat = TestCategoryModel.get_test_category()
            cat.save()

            s = Service(
                category=cat,
                name='Reddit',
                short_name=str(ServiceEnum.REDDIT)
            )
            s.save()

            return s


if __name__ == '__main__':
    unittest.main()
