# -*- coding: utf-8 -*-
import logging

from mock import Mock

from common.enums import StatType
from common.models import UserStat
from robot.models import UserServiceProfile
from robot.worker.instagram.user import UserInstagramWorker
from test.robot.worker.instagram.test_worker import InstagramWorkerTestCase
from test.robot.worker.mock_loader import MockLoader


log = logging.getLogger(__name__)


class UserInstagramWorkerTest(InstagramWorkerTestCase):
    def setUp(self):
        super(UserInstagramWorkerTest, self).setUp()

        self.worker = UserInstagramWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/instagram/json/users_id.json'))
        self.worker.sub.profile = UserServiceProfile.objects.profile_for_username(self.sub.profile.user, self.sub.source.service, 'snoopdogg')
        self.worker.run()

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 0)

    def test_indexed(self):
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.FOLLOWING), 420)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.FOLLOWERS), 3410)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.POSTS), 1320)