# -*- coding: utf-8 -*-
import logging

from mock import Mock

from common.enums import MediaType
from robot.models import Item
from robot.worker.instagram.location import LocationInstagramWorker
from test.robot.worker.instagram.test_worker import InstagramWorkerTestCase
from test.robot.worker.mock_loader import MockLoader


log = logging.getLogger(__name__)


class LocationInstagramWorkerTest(InstagramWorkerTestCase):
    def setUp(self):
        super(InstagramWorkerTestCase, self).setUp()

        self.worker = LocationInstagramWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/instagram/json/location_photos.json'))
        self.worker.run()

    def check_item(self, item):
        self.assertEqual(item.media_type, str(MediaType.PHOTO))
        # self.assertEqual(item.item_type, str(ItemType.INSTAGRAM_LOCATION))
        self.assertEqual(item.url, 'http://instagram.com/p/q9f12-Ovgv/')
        self.assertEqual(item.image,
                         'http://scontent-a.cdninstagram.com/hphotos-xpf1/t51.2885-15/s306x306/e15/10533293_564477097008996_166026070_n.jpg')
        self.assertEqual(item.image_large,
                         'http://scontent-a.cdninstagram.com/hphotos-xpf1/t51.2885-15/e15/10533293_564477097008996_166026070_n.jpg')
        self.assertEqual(item.image_small,
                         'http://scontent-a.cdninstagram.com/hphotos-xpf1/t51.2885-15/s150x150/e15/10533293_564477097008996_166026070_n.jpg')
        self.assertEqual(item.title,
                         u'A felicidade e o amor vivem de m\xe3os dadas em cima de uma linha muito t\xeanue que separa a raz\xe3o da loucura. Edir Porto \u2665\ufe0f')
        self.assertEqual(item.author, 'jakelines2andrade')
        self.assertEqual(item.comments, 0)
        self.assertEqual(item.points, 29)
        self.assertEqual(item.location[0], 41.1455)
        self.assertEqual(item.location[1], -73.9949)

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 1)
        self.check_item(self.worker.items[0])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 1)

        item_0 = items[0]
        self.check_item(item_0)