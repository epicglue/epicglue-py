# -*- coding: utf-8 -*-
import logging

from mock import Mock

from common.enums import MediaType
from robot.worker.instagram.popular import PopularInstagramWorker
from robot.models import Item
from test.robot.worker.instagram.test_worker import InstagramWorkerTestCase
from test.robot.worker.mock_loader import MockLoader


log = logging.getLogger(__name__)


class PopularInstagramWorkerTest(InstagramWorkerTestCase):
    def setUp(self):
        super(PopularInstagramWorkerTest, self).setUp()

        self.worker = PopularInstagramWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/instagram/json/popular.json'))
        self.worker.run()

    def check_image(self, item):
        # self.assertEqual(item.item_type, str(ItemType.INSTAGRAM_POPULAR))
        self.assertEqual(item.media_type, str(MediaType.PHOTO))
        self.assertEqual(item.author, 'it_girls')
        self.assertEqual(item.url, 'http://instagram.com/p/ypCBJbopiq/')
        self.assertEqual(item.title, u'Stylish kids via @chique_le_frique')
        self.assertIsNone(item.description)
        self.assertIsNone(item.content_updated_at)
        self.assertEqual(item.content_id, '912269274459052202_43327213')
        self.assertEqual(item.image,
                         'http://scontent-a.cdninstagram.com/hphotos-xfa1/t51.2885-15/s306x306/e15/10843697_1519179904973399_525780356_n.jpg')
        self.assertEqual(item.image_small,
                         'http://scontent-a.cdninstagram.com/hphotos-xfa1/t51.2885-15/s150x150/e15/10843697_1519179904973399_525780356_n.jpg')
        self.assertEqual(item.image_large,
                         'http://scontent-a.cdninstagram.com/hphotos-xfa1/t51.2885-15/e15/10843697_1519179904973399_525780356_n.jpg')
        # self.assertSetEqual(item.tags, set())

    def check_video(self, item):
        # self.assertEqual(item.item_type, str(ItemType.INSTAGRAM_POPULAR))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.author, 'blackjaguarwhitetiger')
        self.assertEqual(item.url, 'http://instagram.com/p/ypDdAgzGrE/')
        self.assertEqual(item.title,
                         u'Good morning from Maya and Julio...\nBuenos d\xedas de #babymaya y de #babyjulio \n#savelions #saveourplanet')
        self.assertIsNone(item.description)
        self.assertIsNone(item.content_updated_at)
        self.assertEqual(item.content_id, '912275587073862340_223049889')
        self.assertEqual(item.image,
                         'http://scontent-a.cdninstagram.com/hphotos-xaf1/t51.2885-15/s306x306/e15/10958226_625518364220162_273760936_n.jpg')
        self.assertEqual(item.image_small,
                         'http://scontent-a.cdninstagram.com/hphotos-xaf1/t51.2885-15/s150x150/e15/10958226_625518364220162_273760936_n.jpg')
        self.assertEqual(item.image_large,
                         'http://scontent-a.cdninstagram.com/hphotos-xaf1/t51.2885-15/e15/10958226_625518364220162_273760936_n.jpg')
        # self.assertSetEqual(item.tags, set(('babyjulio', 'saveourplanet', 'babymaya', 'savelions')))

    def check_location(self, item):
        self.assertEqual(item.location[0], -37.817227404)
        self.assertEqual(item.location[1], 144.953377563)

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 19)
        self.check_image(self.worker.items[0])
        self.check_video(self.worker.items[2])
        self.check_location(self.worker.items[4])

    def test_stored(self):
        items = Item.objects.all().order_by('created_at')

        self.assertEqual(items.count(), 19)

        self.check_image(items[0])
        self.check_video(items[2])
        self.check_location(items[4])