# -*- coding: utf-8 -*-
import logging

from mock import Mock

from common.enums import ItemType
from common.enums import MediaType
from robot.models import Item
from robot.worker.instagram.feed import FeedInstagramWorker
from test.robot.worker.instagram.test_worker import InstagramWorkerTestCase
from test.robot.worker.mock_loader import MockLoader


log = logging.getLogger(__name__)


class FeedInstagramWorkerTest(InstagramWorkerTestCase):
    def setUp(self):
        super(InstagramWorkerTestCase, self).setUp()

        self.worker = FeedInstagramWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/instagram/json/users_self_feed.json'))
        self.worker.run()

    def check_image_item(self, item):
        self.assertEqual(item.item_type, str(ItemType.INSTAGRAM_POST))
        self.assertEqual(item.media_type, str(MediaType.PHOTO))
        self.assertEqual(item.author, 'seanklingelhoefer')
        self.assertEqual(item.url, 'http://instagram.com/p/vRf1HlKBH5/')
        self.assertEqual(item.title,
                         "It's starting to become a little freaky that new promos are always off the press 24 hours before an international flight. #newpromos #foxcreative #nextstoptokyo")
        self.assertIsNone(item.description)
        self.assertIsNone(item.content_updated_at)
        self.assertEqual(item.content_id, '851601794263421433_48027563')
        self.assertEqual(item.image,
                         'http://scontent-b.cdninstagram.com/hphotos-xap1/t51.2885-15/10809942_1524211381155526_431200341_a.jpg')
        self.assertEqual(item.image_small,
                         'http://scontent-b.cdninstagram.com/hphotos-xap1/t51.2885-15/10809942_1524211381155526_431200341_s.jpg')
        self.assertEqual(item.image_large,
                         'http://scontent-b.cdninstagram.com/hphotos-xap1/t51.2885-15/10809942_1524211381155526_431200341_n.jpg')
        self.assertListEqual(item.tags, ['nextstoptokyo', 'foxcreative', 'newpromos'])

    def check_video_item(self, item):
        self.assertEqual(item.item_type, str(ItemType.INSTAGRAM_POST))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.author, 'mrlevine')
        self.assertEqual(item.url, 'http://instagram.com/p/vRfBHIqJaZ/')
        self.assertEqual(item.title, "#2015F150 body assembly #hyperlapse")
        self.assertIsNone(item.description)
        self.assertIsNone(item.content_updated_at)
        self.assertEqual(item.content_id, '851598220372514457_4682596')
        self.assertEqual(item.image,
                         'http://scontent-a.cdninstagram.com/hphotos-xap1/t51.2885-15/10788039_996212267062258_1461341844_a.jpg')
        self.assertEqual(item.image_small,
                         'http://scontent-a.cdninstagram.com/hphotos-xap1/t51.2885-15/10788039_996212267062258_1461341844_s.jpg')
        self.assertEqual(item.image_large,
                         'http://scontent-a.cdninstagram.com/hphotos-xap1/t51.2885-15/10788039_996212267062258_1461341844_n.jpg')
        self.assertListEqual(item.tags, ['2015f150', 'hyperlapse'])

    def check_location_item(self, item):
        self.assertEqual(item.location[0], 34.711863329)
        self.assertEqual(item.location[1], -118.063882934)

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 19)

        self.check_image_item(self.worker.items[0])
        self.check_video_item(self.worker.items[6])
        self.check_location_item(self.worker.items[4])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 19)

        # self.check_image_item(items[0])
        # self.check_video_item(items[6])
        # self.check_location_item(items[4])