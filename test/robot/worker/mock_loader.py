# -*- coding: utf-8 -*-
import logging
import os

import simplejson as json


log = logging.getLogger(__name__)


class MockLoader(object):
    @staticmethod
    def fetch(filename):
        """
        Load JSON by filename

        :param filename:
        :return:
        """

        path = os.path.dirname(os.path.abspath(__file__)) + filename
        json_data = json.load(open(path))

        log.debug('Loading mock json -- %s' % path)

        return json_data