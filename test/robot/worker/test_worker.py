# -*- coding: utf-8 -*-
import logging
import unittest

from common.es import ES
from common.helpers import random_string
from robot.models import Item, Subscription, Source, ItemRaw
from test.models.test_service import TestServiceModel
from test.models.test_service_profile import TestServiceProfileModel
from test.models.test_user import TestUserModel

log = logging.getLogger(__name__)


class WorkerTestCase(unittest.TestCase):
    def get_source(self):
        try:
            return Source.objects.get(service=self.get_service(), script_name=random_string())
        except Source.DoesNotExist:
            s = Source(
                service=self.get_service(),
                script_name=random_string(),
                name=random_string(),
                is_per_user=True
            )
            s.save()

            return s

    def get_service(self):
        s = TestServiceModel.get_test_service()

        return s

    def get_subscription(self):
        u = TestUserModel.get_test_user()
        u.save()

        p = TestServiceProfileModel.get_test_service_profile()
        p.save()

        s = Subscription.add_subscription(self.get_source(), u, p)

        return s

    def setUp(self):
        super(WorkerTestCase, self).setUp()

        Item.objects.all().delete()
        ItemRaw.objects.all().delete()

        self.sub = self.get_subscription()
        self.worker = None

    def tearDown(self):
        super(WorkerTestCase, self).tearDown()

        if self.worker is not None:
            es = ES()
            es.delete(index=self.worker.sub.profile.user.username)


if __name__ == '__main__':
    unittest.main()