# -*- coding: utf-8 -*-
import logging
import unittest
from datetime import datetime

from mock import Mock

from pytz import utc

from common.enums import ItemType, MediaType, Visibility
from robot.worker.twitter.timeline import TimelineTwitterWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.twitter.test_worker import TwitterWorkerTestCase

log = logging.getLogger(__name__)


class TimelineTwitterWorkerTestCase(TwitterWorkerTestCase):
    def setUp(self):
        super(TimelineTwitterWorkerTestCase, self).setUp()

        self.worker = TimelineTwitterWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/twitter/json/statuses_home_timeline.json'))
        self.worker.run()

    def check_item(self, item):
        self.assertEqual(item.content_id, '539508218670895104')
        self.assertEqual(item.title,
                         u'TVs, games, and an off-contract iPhone 6 \u2014 these are the best #CyberMonday deals: http://t.co/MUDWLpbiJV http://t.co/hxoym7QG86')
        self.assertEqual(item.author, 'verge')
        self.assertEqual(item.content_created_at, datetime(2014, 12, 1, 19, 55, 57, tzinfo=utc))
        self.assertEqual(item.points, 4)
        self.assertEqual(item.comments, 6)
        self.assertEqual(item.item_type, str(ItemType.TWITTER_TWEET))
        self.assertEqual(item.media_type, str(MediaType.TWEET))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 20)

        # def test_stored(self):
        #     items = Item.objects.all()
        #
        #     self.assertEqual(items.count(), 20)
        #
        #     self.check_item(items[0])


if __name__ == '__main__':
    unittest.main()
