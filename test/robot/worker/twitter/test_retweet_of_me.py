# -*- coding: utf-8 -*-
import logging
import unittest
from datetime import datetime

from mock import Mock
from pytz import utc

from common.enums import ItemType, MediaType, Visibility
from robot.models import Item
from robot.worker.twitter.retweet_of_me import RetweetOfMeTwitterWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.twitter.test_worker import TwitterWorkerTestCase

log = logging.getLogger(__name__)


class RetweetTwitterWorkerTestCase(TwitterWorkerTestCase):
    def setUp(self):
        super(RetweetTwitterWorkerTestCase, self).setUp()

        self.worker = RetweetOfMeTwitterWorker(self.sub)
        self.worker.fetch = Mock(
            return_value=MockLoader.fetch('/twitter/json/statuses_retweets_of_me.json'))
        self.worker.run()

    def check_item(self, item):
        self.assertEqual(item.content_id, '259320959964680192')
        self.assertEqual(item.title, u'It\'s bring your migraine to work day today!')
        self.assertEqual(item.content_created_at, datetime(2012, 10, 19, 15, 51, 49, tzinfo=utc))
        self.assertEqual(item.author, 'episod')
        self.assertEqual(item.item_type, str(ItemType.TWITTER_TWEET))
        self.assertEqual(item.media_type, str(MediaType.TWEET))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 1)

        self.check_item(self.worker.items[0])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 1)

        self.check_item(items[0])


if __name__ == '__main__':
    unittest.main()
