# -*- coding: utf-8 -*-
import logging
import unittest

from common.enums import Service as ServiceEnum
from robot.models import Service
from test.models.test_category import TestCategoryModel
from test.robot.worker.test_worker import WorkerTestCase


log = logging.getLogger(__name__)


class TwitterWorkerTestCase(WorkerTestCase):
    def get_service(self):
        try:
            return Service.objects.get(short_name=ServiceEnum.TWITTER)
        except Service.DoesNotExist:
            cat = TestCategoryModel.get_test_category()
            cat.save()

            s = Service(
                category=cat,
                name='Twitter',
                short_name=ServiceEnum.TWITTER
            )
            s.save()

            return s


if __name__ == '__main__':
    unittest.main()
