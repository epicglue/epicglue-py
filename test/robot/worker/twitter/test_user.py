# -*- coding: utf-8 -*-
import logging
import unittest

from mock import Mock

from common.enums import StatType
from common.models import UserStat
from robot.models import UserServiceProfile
from robot.worker.twitter.user import UserTwitterWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.twitter.test_worker import TwitterWorkerTestCase

log = logging.getLogger(__name__)


class UserTwitterWorkerTestCase(TwitterWorkerTestCase):
    def setUp(self):
        super(UserTwitterWorkerTestCase, self).setUp()

        self.worker = UserTwitterWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/twitter/json/users_me.json'))
        self.worker.sub.profile = UserServiceProfile.objects.profile_for_username(self.sub.profile.user, self.sub.source.service, 'rsarver')
        self.worker.run()

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 0)

    def test_indexed(self):
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.FOLLOWING), 1780)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.FOLLOWERS), 276334)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.POSTS), 13728)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.LISTED), 1586)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.LIKED), 3162)


if __name__ == '__main__':
    unittest.main()
