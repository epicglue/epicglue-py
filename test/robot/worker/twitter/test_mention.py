# -*- coding: utf-8 -*-
import logging
import unittest
from datetime import datetime

from mock import Mock

from pytz import utc

from common.enums import MediaType, ItemType, Visibility
from robot.models import Item
from robot.worker.twitter.mention import MentionTwitterWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.twitter.test_worker import TwitterWorkerTestCase

log = logging.getLogger(__name__)


class MentionTwitterWorkerTestCase(TwitterWorkerTestCase):
    def setUp(self):
        super(MentionTwitterWorkerTestCase, self).setUp()

        self.worker = MentionTwitterWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/twitter/json/statuses_mentions_timeline.json'))
        self.worker.run()

    def check_item(self, item):
        self.assertEqual(item.content_id, '242613977966850048')
        self.assertEqual(item.title,
                         u"@jasoncosta @themattharris Hey! Going to be in Frisco in October. Was hoping to have a meeting to talk about @thinkwall if you're around?")
        self.assertIsNone(item.description)
        self.assertEqual(item.content_created_at, datetime(2012, 9, 3, 13, 24, 14, tzinfo=utc))
        self.assertEqual(item.points, 0)
        self.assertEqual(item.comments, 0)
        self.assertEqual(item.item_type, str(ItemType.TWITTER_TWEET))
        self.assertEqual(item.media_type, str(MediaType.TWEET))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def check_retweet(self, item):
        self.assertEqual(item.item_type, str(ItemType.TWITTER_RETWEET))
        self.assertEqual(item.points, 2)
        self.assertEqual(item.comments, 3)

    def check_location(self, item):
        self.assertEqual(item.location[0], 121.0132101)
        self.assertEqual(item.location[1], 14.5191613)

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 13)

        self.check_item(self.worker.items[0])
        self.check_retweet(self.worker.items[2])
        self.check_location(self.worker.items[1])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 13)

        self.check_item(items[0])
        self.check_retweet(items[2])
        self.check_location(items[1])


if __name__ == '__main__':
    unittest.main()
