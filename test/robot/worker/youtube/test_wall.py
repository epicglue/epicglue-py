# -*- coding: utf-8 -*-
import logging
import unittest
from datetime import datetime

from mock import Mock
from pytz import utc

from common.enums import MediaType, Visibility, ItemType
from robot.models import Item
from robot.worker.youtube.wall import WallYouTubeWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.youtube.test_worker import YouTubeWorkerTestCase

log = logging.getLogger(__name__)


class WallYouTubeWorkerTestCase(YouTubeWorkerTestCase):
    def setUp(self):
        super(WallYouTubeWorkerTestCase, self).setUp()

        self.worker = WallYouTubeWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/youtube/json/activities.json'))
        self.worker.run()

    def check_upload_item(self, item):
        self.assertEqual(item.content_id, 'VTE0MjM4NjA2NDAxNDA1NjIzOTM4MDMxNTI=')
        self.assertEqual(item.title, u'Do You Need To Be Outgoing To Be An Entrepreneur?')
        self.assertEqual(item.author, 'Gary Vaynerchuk')
        self.assertEqual(item.content_created_at, datetime(2015, 2, 13, 20, 50, 40, tzinfo=utc))
        self.assertEqual(item.description,
                         u"Roommate Harmony Asks: Hey Gary, Do you think it is necessary to have an outgoing personality to be a successful entrepreneur?\n\nRoommate Harmony, this is a tremendous question!  I think it has never been less important to be extroverted to be a successful entrepreneur, or I do not know just look at every successful entrepreneur that everybody talks about, like Zucks, Ev Williams, Kevin Systrom, and David Karp, you know Facebook, Twitter, Tumblr, Pinterest & Instagram.  I mean it is actually the glory days of the introverted entrepreneur because of technology, because of sitting behind the screen, because it doesn't all happen face to face anymore.  It has never been a better time to do that and that is quite by the way, always been the way.  It is you know there are ways to win, you know, it is about betting on strengths.  I do things based on my strengths.  I am an entrepreneur that is extroverted and I saw around myself with people and I do that kind of stuff, I put myself out there, introverted entrepreneurs need to not fake the funk like it would be stupid for me to sit behind the computer all day everyday and then that was just the way I rolled mistake leaving the magic on the table.  Equally, someone who is awkward, and my startup is really like that guy or gal is probably not the kind of person that wants to kind of bet on their personality and they need to sit down all day and focus, and so I think it is betting on strengths.\n\n--\nGary Vaynerchuk is a New York Times and Wall Street Journal Best-Selling author, self-taught wine expert, and innovative entrepreneur. Find more at http://garyvaynerchuk.com\n\nJab, Jab, Jab, Right Hook is now available on Amazon! http://bit.ly/jjjrhamazon")
        self.assertEqual(item.url, 'http://www.youtube.com/watch?v=-ads9HZoZPg')
        self.assertEqual(item.image, 'https://i.ytimg.com/vi/-ads9HZoZPg/default.jpg')
        self.assertEqual(item.image_large, 'https://i.ytimg.com/vi/-ads9HZoZPg/maxresdefault.jpg')
        self.assertEqual(item.item_type, str(ItemType.YOUTUBE_UPLOAD))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def check_like_item(self, item):
        self.assertEqual(item.content_id, 'TElLMTQyNDI2MTM0OTE0MDU2MjM5Mzc4NTIzMg==')
        self.assertEqual(item.title, u'LEARN FOOTBALL SKILLS - SKILL 4 - Amazing Back Heel Pass')
        self.assertEqual(item.author, 'STRskillSchool')
        self.assertEqual(item.content_created_at, datetime(2015, 2, 18, 12, 9, 9, tzinfo=utc))
        self.assertEqual(item.description,
                         u"LEARN AMAZING FOOTBALL SKILLS EVERY WEEK\nSUBSCRIBE NOW  http://goo.gl/4PKgRZ\nBest Football Channel on YouTube\n\nLast video http://youtu.be/sfpP57RKVtk\n\nIf you want more information on how to do this skill, comment or message me on other social media below for a fast reply\n\nFOLLOW ME\nINSTAGRAM: http://instagram.com/STRskillSchool\nTWITTER: http://www.twitter.com/strskillschool @STRskillSchool\nFACEBOOK: http://www.facebook.com/strskillschool\n\nCome join me at Whistle Sports: http://whistlesports.com/\n\n\nSpecial thanks to Walsham Le Willows FC for letting me film at he club.")
        self.assertEqual(item.url, 'http://www.youtube.com/watch?v=oZtmBZYS1WM')
        self.assertEqual(item.image, 'https://i.ytimg.com/vi/oZtmBZYS1WM/default.jpg')
        # self.assertEqual(item.image_large, 'https://i.ytimg.com/vi/oZtmBZYS1WM/sddefault.jpg')
        self.assertEqual(item.item_type, str(ItemType.YOUTUBE_LIKE))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def check_recommendation_item(self, item):
        self.assertEqual(item.content_id, 'UkVDMTQyNDU0NDc1NDE0MDU2MjAxMDM2OTgwOA==')
        self.assertEqual(item.title, u'A. Jesse Jiryu Davis: What Is Async, How Does It Work, And When Should I Use It? - PyCon 2014')
        self.assertEqual(item.author, 'PyCon 2014')
        self.assertEqual(item.content_created_at, datetime(2015, 2, 21, 18, 52, 34, tzinfo=utc))
        self.assertEqual(item.description,
                         u"Speaker: A. Jesse Jiryu Davis\n\nPython's asynchronous frameworks, like Tulip, Tornado, and Twisted, are increasingly important for writing high-performance web applications. Even if you're an experienced web programmer, you may lack a rigorous understanding of how these frameworks work and when to use them. Let's see how Tulip's event loop works, and learn how to efficiently handle very large numbers of concurrent connections.\n\nSlides can be found at: https://speakerdeck.com/pycon2014 and https://github.com/PyCon/2014-slides")
        self.assertEqual(item.item_type, str(ItemType.YOUTUBE_RECOMMENDATION))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def check_bulletin_item(self, item):
        self.assertEqual(item.content_id, 'QlVMMTQyNDM4OTQyNDE0MDU2MjAxMDM2NzY5Ng==')
        self.assertEqual(item.title, u'Galantis - Gold Dust')
        self.assertEqual(item.author, 'Proximity')
        self.assertEqual(item.content_created_at, datetime(2015, 2, 19, 23, 43, 44, tzinfo=utc))
        self.assertEqual(item.description, u"Double upload in one day. Hopefully you all don't mind. ;)")
        self.assertEqual(item.item_type, str(ItemType.YOUTUBE_BULLETIN))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def check_playlist_item(self, item):
        self.assertEqual(item.content_id, 'QTE0MjQxMjUxNzUxNDA1NjIwMDQ5MjYyODg=')
        self.assertEqual(item.title, u'Pokédziałek #48 [16.02] - Pokémon Omega Ruby/Alpha Sapphire')
        self.assertEqual(item.author, 'arhn2eu')
        self.assertEqual(item.content_created_at, datetime(2015, 2, 16, 22, 19, 35, tzinfo=utc))
        self.assertEqual(item.description,
                         u"To już Pokédziałki numer 48 i powoli zbliżamy się do końca gry. Mierzymy się z Elitarną Czwórki i powoli zaczynamy spoglądać ku zachodowi... -- Watch live at http://www.twitch.tv/arhneu")
        self.assertEqual(item.item_type, str(ItemType.YOUTUBE_PLAYLIST_ITEM))
        self.assertEqual(item.media_type, str(MediaType.VIDEO))
        self.assertEqual(item.visibility, str(Visibility.PUBLIC))

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 50)

        self.check_upload_item(self.worker.items[0])
        self.check_like_item(self.worker.items[14])
        self.check_recommendation_item(self.worker.items[5])
        self.check_bulletin_item(self.worker.items[7])
        self.check_playlist_item(self.worker.items[18])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 40)

        # self.check_upload_item(items[0])
        # self.check_like_item(items[14])
        # self.check_recommendation_item(items[5])
        # self.check_bulletin_item(items[7])
        # self.check_playlist_item(items[18])


if __name__ == '__main__':
    unittest.main()
