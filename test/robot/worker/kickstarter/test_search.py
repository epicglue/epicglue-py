# -*- coding: utf-8 -*-
import logging
import unittest

from mock import Mock

from robot.worker.kickstarter.search import SearchKickstarterWorker
from test.robot.worker.kickstarter.test_worker import KickstarterWorkerTestCase
from test.robot.worker.mock_loader import MockLoader

log = logging.getLogger(__name__)


class SearchKickstarterWorkerTestCase(KickstarterWorkerTestCase):
    def setUp(self):
        super(SearchKickstarterWorkerTestCase, self).setUp()

        self.worker = SearchKickstarterWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/kickstarter/json/search.json'))
        self.worker.run()

        # def check_item(self, item):
        #     self.assertEqual(item.item_type, str(ItemType.KICKSTARTER_FUND))
        #     self.assertEqual(item.media_type, str(MediaType.AUCTION))
        #     self.assertEqual(item.visibility, str(Visibility.PUBLIC))
        #
        #     self.assertEqual(item.title, 'The List - HARDCOVER SPECIAL EDITION')
        #     self.assertEqual(item.content_id, '629114323')
        #     self.assertEqual(item.author, 'Adam Jack')
        #     self.assertEqual(item.url, 'https://www.kickstarter.com/projects/1547307744/the-list-hardcover-special-edition?ref=ending_soon')
        #
        # def test_fetch(self):
        #     self.assertEqual(len(self.worker.items), 20)
        #
        #     self.check_item(self.worker.items[0])
        #
        # def test_stored(self):
        #     items = Item.objects.all()
        #
        #     self.assertEqual(items.count(), 20)
        #
        #     self.check_item(items[0])


if __name__ == '__main__':
    unittest.main()
