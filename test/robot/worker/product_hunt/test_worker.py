# -*- coding: utf-8 -*-
import logging
import unittest

from robot.models import Service
from test.models.test_category import TestCategoryModel
from test.robot.worker.test_worker import WorkerTestCase


log = logging.getLogger(__name__)


class ProductHuntWorkerTestCase(WorkerTestCase):
    def get_service(self):
        try:
            return Service.objects.get(short_name='product_hunt')
        except Service.DoesNotExist:
            cat = TestCategoryModel.get_test_category()
            cat.save()

            s = Service(
                category=cat,
                name='Product Hunt',
                short_name='product_hunt'
            )
            s.save()

            return s


if __name__ == '__main__':
    unittest.main()