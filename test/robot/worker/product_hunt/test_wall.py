# -*- coding: utf-8 -*-
import logging
import unittest

from mock import Mock

from common.enums import ItemType, MediaType
from robot.models import Item
from robot.worker.product_hunt.post import PostProductHuntWorker
from test.robot.worker.mock_loader import MockLoader
from test.robot.worker.product_hunt.test_worker import ProductHuntWorkerTestCase

log = logging.getLogger(__name__)


class WallProductHuntWorkerTestCase(ProductHuntWorkerTestCase):
    def setUp(self):
        super(WallProductHuntWorkerTestCase, self).setUp()

        self.worker = PostProductHuntWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/product_hunt/json/posts.json'))
        self.worker.run()

    def check_item(self, item):
        self.assertEqual(item.item_type, str(ItemType.PRODUCT_HUNT_POST))
        self.assertEqual(item.media_type, str(MediaType.LINK))
        self.assertEqual(item.content_id, '2')
        self.assertEqual(item.title, u'Awesome Idea #4')
        self.assertEqual(item.description, u'Great new search engine')
        self.assertEqual(item.comments, 3)
        self.assertEqual(item.points, 2)
        self.assertEqual(item.url, 'http://www.producthunt.com/posts/awesome-idea-4')
        self.assertEqual(item.author, 'producthunter22')
        self.assertEqual(item.image, 'http://placehold.it/850x850.png')
        self.assertEqual(item.image_small, 'http://placehold.it/850x850.png')
        self.assertEqual(item.image_large, 'http://placehold.it/850x850.png')

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 2)

        self.check_item(self.worker.items[0])

    def test_stored(self):
        items = Item.objects.all()

        self.assertEqual(items.count(), 2)

        self.check_item(items[0])


if __name__ == '__main__':
    unittest.main()