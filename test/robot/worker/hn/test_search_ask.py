# -*- coding: utf-8 -*-
import logging
import unittest
from datetime import datetime

from mock import Mock
from pytz import utc

from common.enums import MediaType
from robot.models import Item
from robot.worker.hacker_news.search import SearchHackerNewsWorker
from test.robot.worker.hn.test_worker import HnWorkerTestCase
from test.robot.worker.mock_loader import MockLoader

log = logging.getLogger(__name__)


class SearchHnWorkerTestCase(HnWorkerTestCase):
    def setUp(self):
        super(SearchHnWorkerTestCase, self).setUp()

        self.worker = SearchHackerNewsWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/hn/json/search_ask.json'))
        self.worker.run()

    def check_item(self, item):
        # self.assertEqual(item.item_type, str(ItemType.HACKER_NEWS_STORY))
        self.assertEqual(item.media_type, str(MediaType.LINK))
        self.assertEqual(item.title, u'Ask HN: Urgent connection to Twitter support')
        self.assertEqual(item.description,
                         u'HN,For the past few hours Israel and Gaza have been exchanging rocket and missile attacks, with many casualties on both sides.I am the admin for an automated emergency alert twitter feed that thousands of users depend on to receive alerts on incoming attacks.Currently, we are approaching API limits and require immediate relaxation of some of the limits.Support tickets have been filed, but no answers just yet, and escalation is required.Any connections to Twitter support staff that can help us expedite handling of this ticket would be awesome, and would be helpful to many who depend on this feed during this difficult time.Thanks,\\n@yuvadm')
        self.assertEqual(item.author, 'yuvadam')
        self.assertEqual(item.url, 'https://news.ycombinator.com/item?id=8006869')
        self.assertEqual(item.content_id, '8006869')
        self.assertEqual(item.content_created_at, datetime(2014, 7, 8, 21, 37, 52, tzinfo=utc))
        self.assertEqual(item.points, 874)
        self.assertEqual(item.comments, 130)

        # set("story", "author_yuvadam", "story_8006869", "ask_hn")

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 20)

        self.check_item(self.worker.items[0])

    def test_stored(self):
        items = Item.objects.all().order_by('-points')

        self.assertEqual(items.count(), 20)

        self.check_item(items[0])


if __name__ == '__main__':
    unittest.main()
