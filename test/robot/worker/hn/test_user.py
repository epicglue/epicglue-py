# -*- coding: utf-8 -*-
import logging
import unittest

from mock import Mock

from common.enums import StatType
from common.models import UserStat
from robot.models import UserServiceProfile
from robot.worker.hacker_news.user import UserHackerNewsWorker
from test.robot.worker.hn.test_worker import HnWorkerTestCase
from test.robot.worker.mock_loader import MockLoader

log = logging.getLogger(__name__)


class UserHnWorkerTestCase(HnWorkerTestCase):
    def setUp(self):
        super(UserHnWorkerTestCase, self).setUp()

        self.worker = UserHackerNewsWorker(self.sub)
        self.worker.fetch = Mock(return_value=MockLoader.fetch('/hn/json/user.json'))
        self.worker.sub.profile = UserServiceProfile.objects.profile_for_username(self.sub.profile.user, self.sub.source.service, 'pg')
        self.worker.run()

    def test_fetch(self):
        self.assertEqual(len(self.worker.items), 0)

    def test_indexed(self):
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.KARMA), 155046)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.POSTS), 645)
        self.assertEqual(UserStat.objects.get_latest_value(self.sub.profile, StatType.COMMENTS), 8806)


if __name__ == '__main__':
    unittest.main()
