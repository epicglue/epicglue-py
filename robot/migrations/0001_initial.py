# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
from django.conf import settings
import django_extensions.db.fields.json


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('description', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('is_visible', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('upper_category', models.ForeignKey(default=None, blank=True, to='robot.Category', null=True)),
            ],
            options={
                'db_table': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feedback', models.TextField(editable=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'feedback',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('hash', models.CharField(max_length=64, serialize=False, primary_key=True)),
                ('service', models.CharField(max_length=30)),
                ('title', models.TextField(default=None, null=True, blank=True)),
                ('description', models.TextField(default=None, null=True, blank=True)),
                ('tags', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=None, null=True, blank=True), size=None)),
                ('item_type', models.CharField(max_length=30)),
                ('media_type', models.CharField(max_length=30)),
                ('author', models.TextField()),
                ('author_image', models.TextField(default=None, null=True, blank=True)),
                ('author_image_large', models.TextField(default=None, null=True, blank=True)),
                ('image', models.TextField(default=None, null=True, blank=True)),
                ('image_large', models.TextField(default=None, null=True, blank=True)),
                ('image_small', models.TextField(default=None, null=True, blank=True)),
                ('image_width', models.IntegerField(default=None, null=True, blank=True)),
                ('image_height', models.IntegerField(default=None, null=True, blank=True)),
                ('image_large_width', models.IntegerField(default=None, null=True, blank=True)),
                ('image_large_height', models.IntegerField(default=None, null=True, blank=True)),
                ('image_small_width', models.IntegerField(default=None, null=True, blank=True)),
                ('image_small_height', models.IntegerField(default=None, null=True, blank=True)),
                ('video', models.TextField(default=None, null=True, blank=True)),
                ('video_width', models.IntegerField(default=None, null=True, blank=True)),
                ('video_height', models.IntegerField(default=None, null=True, blank=True)),
                ('video_large', models.TextField(default=None, null=True, blank=True)),
                ('video_large_width', models.IntegerField(default=None, null=True, blank=True)),
                ('video_large_height', models.IntegerField(default=None, null=True, blank=True)),
                ('location', django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(default=None, null=True, blank=True), size=2)),
                ('location_name', models.TextField(default=None, null=True, blank=True)),
                ('url', models.TextField(default=None, null=True, blank=True)),
                ('url_in', models.TextField(default=None, null=True, blank=True)),
                ('url_ext', models.TextField(default=None, null=True, blank=True)),
                ('item_hash', models.CharField(default=None, max_length=64, null=True, blank=True)),
                ('content_id', models.CharField(max_length=100)),
                ('content_secondary_id', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('content_created_at', models.DateTimeField()),
                ('content_updated_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('content_order_by_date', models.DateTimeField()),
                ('visibility', models.CharField(max_length=10)),
                ('points', models.IntegerField(default=None, null=True, blank=True)),
                ('comments', models.IntegerField(default=None, null=True, blank=True)),
                ('is_duplicate', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'item',
            },
        ),
        migrations.CreateModel(
            name='ItemRaw',
            fields=[
                ('hash', models.CharField(max_length=64, serialize=False, primary_key=True)),
                ('json', django_extensions.db.fields.json.JSONField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'item_raw',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('short_name', models.SlugField(unique=True, max_length=30)),
                ('description', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('is_locked', models.BooleanField(default=False)),
                ('is_visible', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(to='robot.Category')),
            ],
            options={
                'db_table': 'service',
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('script_name', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=30)),
                ('description', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('url', models.CharField(max_length=200)),
                ('item_ordering_field', models.CharField(default=b'content_created_at', max_length=20)),
                ('allow_value', models.BooleanField(default=False)),
                ('value', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('value_hint', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('min_refresh', models.IntegerField(default=300)),
                ('do_auto_subscribe', models.BooleanField(default=False)),
                ('is_locked', models.BooleanField(default=False)),
                ('is_per_user', models.BooleanField(default=True)),
                ('is_active', models.BooleanField(default=True)),
                ('is_public', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('service', models.ForeignKey(to='robot.Service')),
            ],
            options={
                'db_table': 'source',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'state',
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('status', models.CharField(default=b'ready', max_length=15, choices=[(b'ready', b'Ready'), (b'enqueued', b'Enqueued'), (b'progress', b'In Progress'), (b'failed', b'Failed')])),
                ('tokens_used', models.IntegerField(default=0, null=True, blank=True)),
                ('tokens_left', models.IntegerField(default=0, null=True, blank=True)),
                ('tokens_reset_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('overwritten_by_subscription_id', models.IntegerField(default=None, null=True, blank=True)),
                ('due_refresh', models.BooleanField(default=True)),
                ('is_active', models.BooleanField(default=True)),
                ('is_hidden', models.BooleanField(default=False)),
                ('last_refresh_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'subscription',
            },
        ),
        migrations.CreateModel(
            name='SubscriptionLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.TextField(default=None, null=True, blank=True)),
                ('code', models.IntegerField()),
                ('msg', models.TextField(default=None, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('subscription', models.ForeignKey(to='robot.Subscription')),
            ],
            options={
                'db_table': 'subscription_log',
                'verbose_name': 'Subscription Log',
            },
        ),
        migrations.CreateModel(
            name='UserItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_read', models.BooleanField(default=False)),
                ('is_glued', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('item', models.ForeignKey(to='robot.Item')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_item',
            },
        ),
        migrations.CreateModel(
            name='UserItemSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_indexed', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user_item', models.ForeignKey(to='robot.UserItem')),
            ],
            options={
                'db_table': 'user_item_subscription',
            },
        ),
        migrations.CreateModel(
            name='UserServiceProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=50)),
                ('friendly_name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('service', models.ForeignKey(to='robot.Service')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_service_profile',
            },
        ),
        migrations.CreateModel(
            name='UserServiceToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('scope', models.CharField(max_length=255)),
                ('token', models.CharField(max_length=255)),
                ('token_secret', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('refresh_token', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('expiry', models.DateTimeField(default=None, null=True, blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('profile', models.ForeignKey(default=None, blank=True, to='robot.UserServiceProfile', null=True)),
            ],
            options={
                'db_table': 'user_service_token',
            },
        ),
        migrations.CreateModel(
            name='UserSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('profile', models.ForeignKey(default=None, blank=True, to='robot.UserServiceProfile', null=True)),
                ('subscription', models.ForeignKey(to='robot.Subscription')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_subscription',
                'verbose_name': "user's subscription",
            },
        ),
        migrations.AddField(
            model_name='useritemsubscription',
            name='user_subscription',
            field=models.ForeignKey(to='robot.UserSubscription'),
        ),
        migrations.AddField(
            model_name='subscription',
            name='profile',
            field=models.ForeignKey(default=None, blank=True, to='robot.UserServiceProfile', null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='source',
            field=models.ForeignKey(to='robot.Source'),
        ),
        migrations.AddField(
            model_name='subscription',
            name='user',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='state',
            name='subscription',
            field=models.ForeignKey(to='robot.Subscription'),
        ),
        migrations.AlterUniqueTogether(
            name='usersubscription',
            unique_together=set([('subscription', 'user', 'profile')]),
        ),
        migrations.AlterUniqueTogether(
            name='userserviceprofile',
            unique_together=set([('user', 'service', 'identifier')]),
        ),
        migrations.AlterUniqueTogether(
            name='useritem',
            unique_together=set([('user', 'item')]),
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('source', 'value')]),
        ),
        migrations.AlterIndexTogether(
            name='subscription',
            index_together=set([('due_refresh', 'status', 'is_active')]),
        ),
        migrations.AlterUniqueTogether(
            name='source',
            unique_together=set([('service', 'script_name', 'value')]),
        ),
    ]
