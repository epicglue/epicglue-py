# -*- coding: utf-8 -*-
import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.models import Source, Subscription
from robot.worker.youtube.single_item import SingleItemYouTubeWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, item_id):
        src = Source.objects.get(script_name='youtube_worker')
        sub = Subscription.objects.get(source=src)
        sub.value = item_id

        SingleItemYouTubeWorker(sub).run()
