# -*- coding: utf-8 -*-
import logging
import datetime
import time
import subprocess

from django.conf import settings
from django.core.management import BaseCommand

from common.enums import SubscriptionStatus
from common.helpers import now
from common.runnable import Runnable
from robot.models import Subscription

log = logging.getLogger(__name__)

SLEEP = 15


class Command(BaseCommand):
    # args = '<poll_id poll_id ...>'
    # help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        task_finder = TaskFinder()
        task_finder.run()


class TaskFinder(Runnable):
    def run(self):
        while True:
            self.stale_tasks()
            self.run_subs()

            time.sleep(SLEEP)

    @staticmethod
    def run_subs():
        Subscription.objects.find_and_update_ready_to_refresh()

        for sub in Subscription.objects.next_to_run():
            subprocess.Popen(["python", "./manage.py", sub.source.script_name, str(sub.id), "--settings=config.%s" % settings.CONFIG_NAME])

            print('Task %s executed' % sub.id)

    @staticmethod
    def stale_tasks():
        for sub in Subscription.objects.filter(updated_at__lt=now() - datetime.timedelta(minutes=5)).exclude(status=str(SubscriptionStatus.READY)):
            sub.status = str(SubscriptionStatus.READY)
            sub.save()

if __name__ == '__main__':
    Command().handle()