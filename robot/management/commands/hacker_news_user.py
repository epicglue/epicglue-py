# -*- coding: utf-8 -*-
import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.hacker_news.user import UserHackerNewsWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        UserHackerNewsWorker(sub).run()
