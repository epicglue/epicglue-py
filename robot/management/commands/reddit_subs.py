import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.reddit.sub import SubRedditWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        SubRedditWorker(sub).run()
