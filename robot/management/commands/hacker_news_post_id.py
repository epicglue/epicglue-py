# -*- coding: utf-8 -*-
import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.hacker_news.post_id import PostIDHackerNewsWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        PostIDHackerNewsWorker(sub).run()
