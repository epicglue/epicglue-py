import logging

from django.core.management import BaseCommand, CommandError

from robot.models import Subscription, Item

log = logging.getLogger(__name__)


class SingleItemWorkerCommand(BaseCommand):
    help = 'Instagram Feed'

    def add_arguments(self, parser):
        parser.add_argument('sub_id', nargs='+', type=int)
        parser.add_argument('item_id', nargs='+', type=basestring)

    def handle(self, *args, **options):
        # TODO: What sub_id is for?!

        for sub_id in options['sub_id']:
            try:
                sub = Subscription.objects.get(pk=sub_id)
            except Subscription.DoesNotExist:
                raise CommandError('Subscription "%s" does not exist' % sub_id)

        for item_id in options['item_id']:
            try:
                item = Item.objects.get(pk=item_id)
            except Item.DoesNotExist:
                raise CommandError('Item "%s" does not exist' % item_id)

        print("Run %s" % item_id)

        self.run(sub)

    def run(self, sub):
        print("run")
        pass
