import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.reddit.post import PostRedditWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        PostRedditWorker(sub).run()
