import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.twitter.retweet_of_me import RetweetOfMeTwitterWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        RetweetOfMeTwitterWorker(sub).run()
