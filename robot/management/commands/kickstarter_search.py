import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.kickstarter.search import SearchKickstarterWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        SearchKickstarterWorker(sub).run()
