import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.twitter.timeline import TimelineTwitterWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        TimelineTwitterWorker(sub).run()
