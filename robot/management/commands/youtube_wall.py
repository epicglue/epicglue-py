import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.youtube.wall import WallYouTubeWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        WallYouTubeWorker(sub).run()
