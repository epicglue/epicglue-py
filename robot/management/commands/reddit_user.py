import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.reddit.user import UserRedditWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        UserRedditWorker(sub).run()
