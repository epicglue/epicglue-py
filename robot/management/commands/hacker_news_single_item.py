# -*- coding: utf-8 -*-
import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.models import Source, Subscription
from robot.worker.hacker_news.single_item import SingleItemHackerNewsWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, item_id):
        src = Source.objects.get(script_name='hacker_news_post')
        sub = Subscription.objects.get(source=src)
        sub.value = item_id

        SingleItemHackerNewsWorker(sub).run()
