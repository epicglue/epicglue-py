import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.instagram.feed import FeedInstagramWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        FeedInstagramWorker(sub).run()
