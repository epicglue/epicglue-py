import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.product_hunt.post import PostProductHuntWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        PostProductHuntWorker(sub).run()
