import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.instagram.popular import PopularInstagramWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        PopularInstagramWorker(sub).run()
