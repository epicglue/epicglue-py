import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.instagram.location import LocationInstagramWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        LocationInstagramWorker(sub).run()
