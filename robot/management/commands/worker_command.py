import logging

from django.core.management import BaseCommand, CommandError

from robot.models import Subscription

log = logging.getLogger(__name__)


class WorkerCommand(BaseCommand):
    help = 'Instagram Feed'

    def add_arguments(self, parser):
        parser.add_argument('sub_id', nargs='+', type=int)

    def handle(self, *args, **options):
        for sub_id in options['sub_id']:
            try:
                sub = Subscription.objects.get(pk=sub_id)
            except Subscription.DoesNotExist:
                raise CommandError('Subscription "%s" does not exist' % sub_id)

            print("Run %s(%d)" % (self.__class__.__name__, sub_id))
            self.run(sub)

    def run(self, sub):
        print("run")
        pass
