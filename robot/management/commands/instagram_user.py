import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.instagram.user import UserInstagramWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        UserInstagramWorker(sub).run()
