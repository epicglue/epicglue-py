import logging

from robot.management.commands.worker_command import WorkerCommand
from robot.worker.twitter.user import UserTwitterWorker

log = logging.getLogger(__name__)


class Command(WorkerCommand):
    def run(self, sub):
        UserTwitterWorker(sub).run()
