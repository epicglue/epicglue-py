from django.contrib import admin

from models import UserServiceToken, Service, Category, Subscription, State, UserSubscription, Source, Item, SubscriptionLog, \
    UserServiceProfile, \
    Feedback


class ItemAdmin(admin.ModelAdmin):
    list_filter = ['service', 'item_type', 'media_type']
    list_display = ['title', 'service', 'author', 'item_type', 'media_type', 'url']


class UserServiceTokenAdmin(admin.ModelAdmin):
    list_filter = ['profile', 'is_active']
    list_display = ['profile', 'token', 'is_active']
    ordering = ['is_active']


class UserServiceProfileAdmin(admin.ModelAdmin):
    list_filter = ['service']
    list_display = ['user', 'service', 'identifier', 'friendly_name']


class CategoryAdmin(admin.ModelAdmin):
    list_filter = ['is_visible']
    list_display = ['name', 'upper_category', 'is_visible']


class ServiceAdmin(admin.ModelAdmin):
    list_filter = ['is_locked', 'is_visible']
    list_display = ['name', 'category', 'is_locked', 'is_visible']
    ordering = ['-is_visible', 'name']


class SourceAdmin(admin.ModelAdmin):
    list_filter = ['service', 'is_per_user', 'is_active', 'allow_value', 'is_locked', 'is_public', 'do_auto_subscribe']
    list_display = ['name', 'service', 'script_name', 'value', 'is_per_user', 'is_locked', 'is_active']
    ordering = ['service', 'is_active', 'name']


class SubscriptionAdmin(admin.ModelAdmin):
    list_filter = ['source', 'due_refresh', 'status', 'is_active']
    list_display = ['source', 'status', 'value', 'is_active']


class SubscriptionLogAdmin(admin.ModelAdmin):
    list_display = ['subscription', 'url', 'code']


class UserSubscriptionAdmin(admin.ModelAdmin):
    list_display = ['subscription', 'profile']


class StateAdmin(admin.ModelAdmin):
    list_display = ['subscription', 'value']


class FeedbackAdmin(admin.ModelAdmin):
    pass


admin.site.register(Item, ItemAdmin)
admin.site.register(UserServiceToken, UserServiceTokenAdmin)
admin.site.register(UserServiceProfile, UserServiceProfileAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(SubscriptionLog, SubscriptionLogAdmin)
admin.site.register(UserSubscription, UserSubscriptionAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Feedback, FeedbackAdmin)
