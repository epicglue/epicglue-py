# -*- coding: utf-8 -*-
import logging
from urllib import quote_plus
import time

import requests

from common.enums import ConflictPolicy
from common.enums import SubscriptionStatus
from common.exception.no_tokens_left_exception import NoTokensLeftException
from common.helpers import now, s_to_qs, s_to_string
from common.runnable import Runnable
from robot.models import Item, State, Subscription, SubscriptionLog, ItemRaw, UserItem, UserItemSubscription

log = logging.getLogger(__name__)


class Worker(Runnable):
    sub = None
    parser = None
    service = None
    state = None
    items = []
    json = []

    url = None
    next_url = None
    custom_headers = None

    data_key = None
    error_key = None

    conflict_policy = ConflictPolicy.REPLACE
    expected_item_count = None
    re_run_on_full_batch = False
    use_state = True
    content_order_field = 'content_created_at'

    def __init__(self, subscription):
        """

        :param subscription: Subscription or int
        :return:
        """
        subscription = self.load_subscription(subscription)

        """:rtype : Subscription"""
        self.sub = subscription
        self.service = self.sub.source.service.short_name
        self.url = self.parse_url()

    def run(self):
        start = time.clock()

        try:
            self.pre()
        except NoTokensLeftException, ex:
            log.error(ex.message)
            return

        if self.use_state:
            self.load_state()

        json = self.fetch()

        if json is not None:
            arr = self.process(json)
            self.items = self.store(arr)

        if self.use_state:
            self.save_state()

        self.post()

        log.info({
            "took": s_to_qs(time.clock() - start),
            "took_str": s_to_string(time.clock() - start),
            "worker": self.__class__.__name__,
            "service": self.service,
            "action": "all"
        })

    def load_subscription(self, subscription):
        if not isinstance(subscription, Subscription):
            try:
                """:type : Subscription"""
                return Subscription.objects.get(pk=subscription)
            except Subscription.DoesNotExist:
                log.critical('Failed to find subscription %s' % subscription)
                raise Exception('Failed to find subscription %s' % subscription)

        return subscription

    def parse_url(self):
        url = self.sub.source.url

        if url is None:
            log.critical('URL not set')

        if url.find('[[value]]') > 0:
            if self.sub.value is not None and len(self.sub.value) > 0:
                url = url.replace('[[value]]', quote_plus(self.sub.value))
            else:
                url = url.replace('[[value]]', quote_plus(self.sub.source.value))

        return url

    # --- Pre
    def pre(self):
        if self.sub.no_tokens_left():
            raise NoTokensLeftException('Could not run %s, no tokens left' % self.__class__.__name__)

        # set status to IN_PROGRESS
        self.sub.status = str(SubscriptionStatus.IN_PROGRESS)
        self.sub.save()

    def load_state(self):
        state = State.for_subscription(self.sub)

        if state.value is not None:
            self.url = state.value

        self.state = state

    # --- Fetch
    def fetch(self):
        start = time.clock()

        log.debug('Fetch %s' % self.url)

        if self.custom_headers is None:
            r = requests.get(self.url)
        else:
            r = requests.get(self.url, headers=self.custom_headers)

        SubscriptionLog.add_log(self.sub, r.status_code, msg=r.text, url=self.url)

        if r.status_code != 200:
            log.error({
                "took": s_to_qs(time.clock() - start),
                "took_str": s_to_string(time.clock() - start),
                "worker": self.__class__.__name__,
                "service": self.service,
                "action": "fetch",
                "message": r.text,
                "status_code": r.status_code
            })

            return self.process_non_200_response(r)

        self.parse_response_headers(r.headers)

        log.info({
            "took": s_to_qs(time.clock() - start),
            "took_str": s_to_string(time.clock() - start),
            "worker": self.__class__.__name__,
            "service": self.service,
            "action": "fetch",
            "response_size": len(r.content),
            "response_size_kb": round(float(len(r.content)) / 1024.0, 2),
        })

        if r.headers.get('content-type') == 'application/json':
            return r.json()

        try:
            return r.json()
        except ValueError:
            return r.text

    def has_errors(self, json):
        return len(self.get_errors(json)) > 0

    def get_errors(self, json):
        return []

    def process_non_200_response(self, r):
        if r.status_code == 401:
            log.warning('Authentication credentials were missing or incorrect. (%s)' % self.url)
        elif r.status_code == 404:
            log.warning('%s Not Found' % self.url)
        elif r.status_code == 410:
            log.warning('%s is gone' % self.url)
        elif r.status_code == 429:
            log.warning('Too many requests. (%s)' % self.url)

            # set tokens to 0 to avoid sending more requests until reset
            log.info('Set tokens from %d to 0' % self.sub.tokens_left)

            self.sub.tokens_left = 0
            self.sub.save()
        elif r.status_code == 500:
            log.warning('%s returned 500' % self.url)
        elif r.status_code == 502 or r.status_code == 503 or r.status_code == 504:
            log.warning('%s is down' % self.url)
        else:
            log.warning('%s returned %d' % (self.url, r.status_code))

    def parse_response_headers(self, headers):
        pass

    # --- Process
    def process(self, json):
        """
        In most case it should be sufficient to set data_key.
        If not method should return list of dicts, ready to convert to Model.fromJSON.

        :return: list
        """

        return []

    # --- Store
    def store(self, arr):
        start = time.clock()

        models = []

        for item in arr:
            model = Item.from_json(item)

            if isinstance(model.tags, set):
                model.tags = list(model.tags)

            model.content_order_by_date = getattr(model, self.content_order_field)
            model.save()

            for user_sub in self.sub.subscribers():
                UserItemSubscription.add_item(UserItem.add_item(user_sub.user, model), user_sub)

            models.append(model)

        log.info({
            "took": s_to_qs(time.clock() - start),
            "took_str": s_to_string(time.clock() - start),
            "worker": self.__class__.__name__,
            "service": self.service,
            "action": "store",
        })

        return models

    # --- Post
    def post(self):
        start = time.clock()

        # Re-run if number of items equals expected_item_count
        if self.expected_item_count is not None and self.items is not None and len(
                self.items) == self.expected_item_count and self.re_run_on_full_batch:
            log.debug('re-run')
            self.sub.due_refresh = True
            self.sub.save()

        # Store raw JSON
        if self.items is not None:
            for idx, model in enumerate(self.items):
                ItemRaw.objects.add_item(hash=model.get_hash(), json=self.json[idx])

        # set status to READY
        self.sub.last_refresh_at = now()
        self.sub.status = str(SubscriptionStatus.READY)
        self.sub.save()

        log.info({
            "took": s_to_qs(float(time.clock() - start)),
            "took_str": s_to_string(float(time.clock() - start)),
            "worker": self.__class__.__name__,
            "service": self.service,
            "action": "post"
        })

    def save_state(self):
        if self.next_url is None:
            log.debug('Next URL not set, no state to store')
            return

        state = State.for_subscription(self.sub)
        state.value = self.next_url
        state.save()

        log.debug('State set to %s' % str(state))

    def get_next_url(self, json):
        return None
