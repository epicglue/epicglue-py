# -*- coding: utf-8 -*-
import logging

from common.enums import ItemType
from robot.parser.product_hunt import Item
from robot.worker.product_hunt.worker import ProductHuntWorker

log = logging.getLogger(__name__)


class PostProductHuntWorker(ProductHuntWorker):
    data_key = 'posts'
    parser = Item()
    item_type = str(ItemType.PRODUCT_HUNT_POST)
    content_order_field = 'created_at'

    def __init__(self, subscription):
        super(PostProductHuntWorker, self).__init__(subscription)

    def get_errors(self, json):
        if 'error' in json:
            return [json['error_description']]

        return []