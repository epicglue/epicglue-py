# -*- coding: utf-8 -*-
import datetime
import logging

from common.helpers import now
from robot.worker.json_worker import JSONWorker

log = logging.getLogger(__name__)


class ProductHuntWorker(JSONWorker):
    error_key = 'error'
    use_state = False

    def __init__(self, sub):
        super(ProductHuntWorker, self).__init__(sub)

        self.custom_headers = {
            'Authorization': 'Bearer ce9dcccd7046c778b3c0dc5b33b0fb77071ae426727d79ec459fc7e34b053dfd',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Host': 'api.producthunt.com'
        }

    def parse_response_headers(self, headers):
        self.sub.tokens_left = int(headers['x-rate-limit-remaining'])
        self.sub.tokens_used = int(headers['x-rate-limit-limit']) - self.sub.tokens_left
        self.sub.tokens_reset_at = now() + datetime.timedelta(int(headers['x-rate-limit-reset']))
        self.sub.save()

    def get_errors(self, json):
        if self.error_key in json:
            return [json['error_description']]

        return []