# -*- coding: utf-8 -*-
import logging
import time

from common.helpers import s_to_string, s_to_qs
from robot.models import UserServiceToken
from robot.worker.worker import Worker

log = logging.getLogger(__name__)


class JSONWorker(Worker):
    def __init__(self, subscription):
        super(JSONWorker, self).__init__(subscription)

    def get_token(self):
        try:
            return UserServiceToken.objects.get(profile=self.sub.profile)
        except UserServiceToken.DoesNotExist:
            return UserServiceToken()

    def process(self, json):
        """
        In most case it should be sufficient to set data_key.
        If not method should return list of dicts, ready to convert to Model.fromJSON.

        :return: list
        """
        start = time.clock()

        if self.has_errors(json):
            log.error(self.get_errors(json))
            raise Exception()

        if isinstance(json, dict) and self.data_key is not None:
            if self.data_key.find('/') >= 0:
                data_json = json
                for chunk in self.data_key.split('/'):
                    data_json = data_json[chunk]
            else:
                data_json = json[self.data_key]
        else:
            data_json = json

        if isinstance(data_json, list) and len(data_json) == 0:
            return []

        items = []

        def build(_item):
            obj = self.parser.parse_item(_item)
            obj['service'] = self.service

            return obj

        if isinstance(data_json, list):
            for item in data_json:
                items.append(build(item))
                self.json.append(item)
        else:
            items.append(build(data_json))
            self.json.append(data_json)

        if len(items) > 0:
            self.next_url = self.get_next_url(json)

        log.info({
            "took": s_to_qs(time.clock() - start),
            "took_str": s_to_string(time.clock() - start),
            "worker": self.__class__.__name__,
            "service": self.service,
            "action": "process"
        })

        return items
