# -*- coding: utf-8 -*-
import logging

from robot.parser.hacker_news import AlgoliaItem
from robot.worker.hacker_news.worker import HackerNewsWorker

log = logging.getLogger(__name__)


class SearchHackerNewsWorker(HackerNewsWorker):
    data_key = 'hits'
    parser = AlgoliaItem()

    def __init__(self, subscription):
        super(SearchHackerNewsWorker, self).__init__(subscription)
