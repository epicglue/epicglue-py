# -*- coding: utf-8 -*-
import logging

import requests

from robot.models import SubscriptionLog, ItemRaw
from robot.parser.hacker_news import HackerNewsItem
from robot.worker.worker import Worker

log = logging.getLogger(__name__)


class PostHackerNewsWorker(Worker):
    parser = HackerNewsItem()

    """
    Not part of Worker
    """

    def __init__(self, subscription):
        self.sub = subscription

    def run(self):
        self.json = self.fetch()
        item = self.process(self.json)
        self.items = self.store([item])
        self.post()

    def fetch(self):
        self.url = self.parse_url()

        r = requests.get(self.url)

        # SubscriptionLog.add_log(self.sub, r.status_code, msg=r.text, url=self.url)

        return r.json()

    def process(self, json):
        item = self.parser.parse_item(json)
        item['service'] = self.sub.source.service.short_name

        return item

    def post(self):
        ItemRaw.objects.add_item(hash=self.items[0].get_hash(), json=self.json)
