# -*- coding: utf-8 -*-
import logging

from common.enums import ItemType
from common.models import UserStat
from robot.parser.hacker_news import User
from robot.worker.hacker_news.worker import HackerNewsWorker

log = logging.getLogger(__name__)


class UserHackerNewsWorker(HackerNewsWorker):
    item_type = str(ItemType.HACKER_NEWS)
    parser = User()

    def __init__(self, subscription):
        super(UserHackerNewsWorker, self).__init__(subscription)

        self.url = 'http://hn.algolia.com/api/v1/users/%s' % self.sub.value

    def store(self, arr):
        # TODO: shouldn't be necessary
        del arr[0]['service']

        UserStat.objects.add_stats(self.sub.profile, arr[0])

        return []