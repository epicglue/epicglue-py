# -*- coding: utf-8 -*-

from robot.worker.json_worker import JSONWorker


class HackerNewsWorker(JSONWorker):
    use_state = False

    def __init__(self, subscription):
        super(HackerNewsWorker, self).__init__(subscription)
