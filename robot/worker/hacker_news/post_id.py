# -*- coding: utf-8 -*-
import logging

from common.enums import ItemType
from robot.models import Subscription, Source, Item
from robot.parser.hacker_news import HackerNewsItem
from robot.worker.hacker_news.post import PostHackerNewsWorker
from robot.worker.hacker_news.worker import HackerNewsWorker

log = logging.getLogger(__name__)


class PostIDHackerNewsWorker(HackerNewsWorker):
    parser = HackerNewsItem()
    content_order_field = 'created_at'

    def __init__(self, subscription):
        super(PostIDHackerNewsWorker, self).__init__(subscription)

        post_source = Source.objects.filter(script_name='hacker_news_post')[0]
        self.post_sub = Subscription(source=post_source)

    def process(self, json):
        """Don't process anything, just pass list of first 30 IDs"""

        items = []

        for post_id in json[:30]:
            item, json = self.process_single_post(post_id)

            if item is None:
                continue

            self.json.append(json)
            items.append(item)

        return items

    def process_single_post(self, post_id):
        self.post_sub.value = str(post_id)

        try:
            Item.objects.get(service=self.service, content_id=str(post_id))
            return None, None
        except Item.DoesNotExist:
            pass

        post = PostHackerNewsWorker(self.post_sub)
        post.run()

        json = post.process(post.json)
        if post.sub.value == 'askstories':
            json['item_type'] = str(ItemType.HACKER_NEWS_ASK)
        elif post.sub.value == 'showstories':
            json['item_type'] = str(ItemType.HACKER_NEWS_SHOW)
        elif post.sub.value == 'jobstories':
            json['item_type'] = str(ItemType.HACKER_NEWS_JOB)
        else:
            json['item_type'] = str(ItemType.HACKER_NEWS_TOP_STORY)

        return json, post.json
