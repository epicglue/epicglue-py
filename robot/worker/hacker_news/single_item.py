# -*- coding: utf-8 -*-
import logging

from robot.models import Item
from robot.parser.hacker_news import HackerNewsItem
from robot.worker.hacker_news.worker import HackerNewsWorker

log = logging.getLogger(__name__)


class SingleItemHackerNewsWorker(HackerNewsWorker):
    parser = HackerNewsItem()

    def __init__(self, subscription):
        super(SingleItemHackerNewsWorker, self).__init__(subscription)

    def process(self, json):
        """Don't process anything, just pass list of first 30 IDs"""

        try:
            Item.objects.get(service=self.service, content_id=str(self.sub.value))
            return []
        except Item.DoesNotExist:
            pass

        item = self.parser.parse_item(json)
        item['service'] = self.service

        self.json.append(json)

        return [item]
