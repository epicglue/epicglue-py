# -*- coding: utf-8 -*-
import logging

from robot.parser.reddit import T3
from robot.worker.reddit.worker import RedditWorker

log = logging.getLogger(__name__)


class SingleItemRedditWorker(RedditWorker):
    data_key = 'data/children'
    parser = T3()
