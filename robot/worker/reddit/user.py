# -*- coding: utf-8 -*-
import logging

from robot.worker.reddit.worker import RedditWorker

log = logging.getLogger(__name__)


class UserRedditWorker(RedditWorker):
    data_key = 'data/children'