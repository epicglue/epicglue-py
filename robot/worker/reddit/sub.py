# -*- coding: utf-8 -*-
import logging

from robot.parser.reddit import T5
from robot.worker.reddit.worker import RedditWorker

log = logging.getLogger(__name__)


class SubRedditWorker(RedditWorker):
    data_key = 'data/children'
    parser = T5()
