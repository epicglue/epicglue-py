# -*- coding: utf-8 -*-
import logging

from common.enums import EpicEnum
from robot.parser.reddit import T3
from robot.worker.json_worker import JSONWorker

log = logging.getLogger(__name__)


class PostGroups(EpicEnum):
    HOT = 'hot'
    NEW = 'new'
    RISING = 'rising'
    CONTROVERSIAL = 'controversial'
    TOP = 'top'
    GILDED = 'gilded'
    PROMOTED = 'ads'


class SubGroups(EpicEnum):
    POPULAR = 'popular'
    NEW = 'new'
    GOLD = 'gold'
    DEFAULT = 'default'


class UserGroups(EpicEnum):
    ABOUT = 'about'
    OVERVIEW = 'overview'
    COMMENTS = 'comments'
    SUBMITTED = 'submitted'
    GILDED = 'gilded'
    UP_VOTED = 'upvoted'
    DOWN_VOTED = 'downvoted'
    HIDDEN = 'hidden'
    SAVED = 'saved'

class RedditWorker(JSONWorker):
    parser = T3()
    # error_key = 'error'

    def __init__(self, sub):
        super(RedditWorker, self).__init__(sub)

        self.custom_headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36'
        }

    def get_errors(self, json):
        if 'data' not in json:
            return [json]

        return []
