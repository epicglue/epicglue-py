# -*- coding: utf-8 -*-
import logging

from robot.worker.json_worker import JSONWorker


log = logging.getLogger(__name__)


class YoutubeWorker(JSONWorker):
    error_key = 'error'

    def __init__(self, sub):
        super(YoutubeWorker, self).__init__(sub)

        self.custom_headers = {
            'Authorization': 'Bearer %s' % self.get_token().token,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Host': 'www.googleapis.com'
        }

    # def parse_response_headers(self, headers):
    # self.sub.tokens_left = int(headers['x-rate-limit-remaining'])
    # self.sub.tokens_used = int(headers['x-rate-limit-limit']) - self.sub.tokens_left
    # self.sub.tokens_reset_at = now() + datetime.timedelta(int(headers['x-rate-limit-reset']))
    #     self.sub.save()

    def get_errors(self, json):
        if self.error_key in json:
            return [json['message']]

        return []