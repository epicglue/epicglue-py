# -*- coding: utf-8 -*-
import logging

from robot.parser.youtube import Item
from robot.worker.youtube.worker import YoutubeWorker

log = logging.getLogger(__name__)


class WallYouTubeWorker(YoutubeWorker):
    parser = Item()
    data_key = 'items'

    def fetch(self):
        if self.state is not None and self.state.value is not None:
            self.url += '&pageToken=%s' % self.state.value

        return super(WallYouTubeWorker, self).fetch()

    def get_next_url(self, json):
        # prevPageToken
        if 'nextPageToken' in json:
            return json['nextPageToken']