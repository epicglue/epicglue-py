# -*- coding: utf-8 -*-
import logging

import tweepy

from common.models import UserStat
from robot.parser.twitter import User
from robot.worker.twitter.worker import TwitterWorker


log = logging.getLogger(__name__)


class UserTwitterWorker(TwitterWorker):
    parser = User()

    def __init__(self, subscription):
        super(UserTwitterWorker, self).__init__(subscription)

    def fetch(self):
        api = self.get_tweepy()

        try:
            me = api.me()
        except tweepy.TweepError as e:
            log.error(e)
            return []

        # self.parse_response_headers(api, 'users', '/users/me')

        items = [me._json]

        return items

    def store(self, arr):
        del arr[0]['service']

        UserStat.objects.add_stats(self.sub.profile, arr[0])

        return []