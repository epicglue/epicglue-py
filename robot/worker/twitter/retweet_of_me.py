# -*- coding: utf-8 -*-
import logging

import tweepy

from robot.parser.twitter import Tweet
from robot.worker.twitter.worker import TwitterWorker


log = logging.getLogger(__name__)


class RetweetOfMeTwitterWorker(TwitterWorker):
    parser = Tweet()
    expected_item_count = 50
    re_run_on_full_batch = True

    def __init__(self, subscription):
        super(RetweetOfMeTwitterWorker, self).__init__(subscription)

    def fetch(self):
        api = self.get_tweepy()

        try:
            if self.state is not None:
                rts = api.retweets_of_me(count=self.expected_item_count, since_id=self.state.value)
            else:
                rts = api.retweets_of_me(count=self.expected_item_count)
        except tweepy.TweepError as e:
            log.error(e)
            return []

        self.parse_response_headers(api, 'statuses', '/statuses/retweets_of_me')

        items = []
        for item in rts:
            items.append(item._json)

        return items