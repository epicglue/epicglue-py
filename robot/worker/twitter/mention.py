# -*- coding: utf-8 -*-
import logging

import tweepy

from robot.parser.twitter import Tweet
from robot.worker.twitter.worker import TwitterWorker


log = logging.getLogger(__name__)


class MentionTwitterWorker(TwitterWorker):
    parser = Tweet()
    expected_item_count = 50
    re_run_on_full_batch = True

    def __init__(self, subscription):
        super(MentionTwitterWorker, self).__init__(subscription)

    def fetch(self):
        api = self.get_tweepy()

        try:
            if self.state is not None:
                mentions = api.mentions_timeline(count=self.expected_item_count, since_id=self.state.value)
            else:
                mentions = api.mentions_timeline(count=self.expected_item_count)
        except tweepy.TweepError as e:
            log.error(e)
            return []

        self.parse_response_headers(api, 'statuses', '/statuses/mentions_timeline')

        items = []
        for item in mentions:
            items.append(item._json)

        return items