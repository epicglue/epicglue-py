# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from pytz import utc
import tweepy
from django.conf import settings

from common.enums import ConflictPolicy
from robot.models import UserServiceToken
from robot.worker.worker import Worker

log = logging.getLogger(__name__)


class TwitterWorker(Worker):
    conflict_policy = ConflictPolicy.IGNORE

    def __init__(self, sub):
        super(TwitterWorker, self).__init__(sub)

    def parse_response_headers(self, api, main_key, key):
        arr = api.rate_limit_status()['resources']

        self.sub.tokens_left = int(arr[main_key][key]['remaining'])
        self.sub.tokens_used = int(arr[main_key][key]['limit']) - self.sub.tokens_left
        self.sub.tokens_reset_at = datetime.fromtimestamp(float(arr[main_key][key]['reset']), tz=utc)
        self.sub.save()

    def get_token(self):
        try:
            return UserServiceToken.objects.get(profile=self.sub.profile)
        except UserServiceToken.DoesNotExist:
            return None

    def get_tweepy(self):
        token = self.get_token()

        auth = tweepy.OAuthHandler(settings.SERVICES['twitter']['clientID'],
                                   settings.SERVICES['twitter']['clientSecret'])
        auth.set_access_token(token.token, token.token_secret)

        return tweepy.API(auth)

    def process(self, json):
        """
        In most case it should be sufficient to set data_key.
        If not method should return list of dicts, ready to convert to Model.fromJSON.

        :return: list
        """

        if self.has_errors(json):
            log.error(self.get_errors(json))
            raise Exception()

        data_json = json[self.data_key] if self.data_key is not None else json

        if isinstance(data_json, list) and len(data_json) == 0:
            return []

        items = []

        def build(item):
            obj = self.parser.parse_item(item)
            obj['service'] = self.service

            return obj

        if isinstance(data_json, list):
            for item in data_json:
                items.append(build(item))
                self.json.append(item)
        else:
            items.append(build(data_json))
            self.json.append(data_json)

        if len(items) > 0:
            self.next_url = self.get_next_url(json)

        return items

    # def get_errors(self, json):
    # if 'errors' in json:
    # return [e['message'] for e in json['errors']]
    #
    # return []

    def get_next_url(self, json):
        """Feed endpoint has issues with min_id if id is out of scope, better to start always with frm beginning, adds overhead though"""
        if isinstance(json, list) and len(json) > 0:
            return json[0]['id']

        return None
