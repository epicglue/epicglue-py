# -*- coding: utf-8 -*-
import logging

import tweepy

from robot.parser.twitter import Tweet
from robot.worker.twitter.worker import TwitterWorker

log = logging.getLogger(__name__)


class SingleItemTwitterWorker(TwitterWorker):
    parser = Tweet()

    def __init__(self, subscription):
        super(SingleItemTwitterWorker, self).__init__(subscription)

    def fetch(self):
        api = self.get_tweepy()

        try:
            statuses = api.statuses_lookup()
        except tweepy.TweepError as e:
            log.error(e)
            return []

        self.parse_response_headers(api, 'statuses', '/statuses/home_timeline')

        items = []
        for item in statuses:
            items.append(item._json)

        return items
