# -*- coding: utf-8 -*-
import logging

import tweepy

from robot.parser.twitter import Tweet
from robot.worker.twitter.worker import TwitterWorker


log = logging.getLogger(__name__)


class TimelineTwitterWorker(TwitterWorker):
    parser = Tweet()
    expected_item_count = 200
    re_run_on_full_batch = True

    def __init__(self, subscription):
        super(TimelineTwitterWorker, self).__init__(subscription)

    def fetch(self):
        api = self.get_tweepy()

        try:
            if self.state is not None:
                timeline = api.home_timeline(count=self.expected_item_count, since_id=self.state.value)
            else:
                timeline = api.home_timeline(count=self.expected_item_count)
        except tweepy.TweepError as e:
            log.error(e)
            return []

        self.parse_response_headers(api, 'statuses', '/statuses/home_timeline')

        items = []
        for item in timeline:
            items.append(item._json)

        return items