# -*- coding: utf-8 -*-
import logging

from common.enums import EpicEnum
from robot.parser.kickstarter import Item
from robot.worker.json_worker import JSONWorker

log = logging.getLogger(__name__)


class Sorting(EpicEnum):
    MAGIC = 'magic'
    POPULARITY = 'popularity'
    NEWEST = 'newest'
    END_DATE = 'end_date'
    MOST_FUNDED = 'most_funded'


class Category(EpicEnum):
    THEATRE = 'theatre'
    TECHNOLOGY = 'technology'
    PUBLISHING = 'publishing'
    PHOTOGRAPHY = 'photography'
    MUSIC = 'music'
    GAMES = 'games'
    FOOD = 'food'
    FILM_AND_VIDEO = 'film & video'
    FASHION = 'fashion'
    DESIGN = 'design'
    DANCE = 'dance'
    COMICS = 'comics'
    ART = 'art'
    HARDWARE = 'hardware'
    JOURNALISM = 'journalism'


class State(EpicEnum):
    LIVE = 'live'
    SUCCESSFUL = 'successful'


class Amount(EpicEnum):
    LESS_THAN_1K = '0'
    BETWEEN_1K_AND_10K = '1'
    BETWEEN_10K_AND_100K = '2'
    BETWEEN_100K_AND_1M = '3'
    OVER_1M = '4'


class Raised(EpicEnum):
    LESS_THAN_75 = '0'
    BETWEEN_75_AND_100 = '1'
    OVER_100 = '2'


class KickstarterWorker(JSONWorker):
    # https://github.com/markolson/kickscraper/issues/16#issuecomment-31409151

    data_key = 'projects'
    parser = Item()
    content_order_field = 'created_at'
