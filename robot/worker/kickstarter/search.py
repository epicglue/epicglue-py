# -*- coding: utf-8 -*-
import logging
from urllib import urlencode

from robot.worker.kickstarter.worker import KickstarterWorker

log = logging.getLogger(__name__)


class SearchKickstarterWorker(KickstarterWorker):
    def __init__(self, subscription):
        super(SearchKickstarterWorker, self).__init__(subscription)

        params = {}
        if subscription.source.value != None and len(subscription.source.value) > 0:
            for var in subscription.source.value.split(','):
                try:
                    k, v = var.split('=')
                except ValueError, e:
                    log.error(subscription.source.value)
                    log.error(var)
                    log.error(e)
                    continue
                params[k] = v
        if subscription.value != None and len(subscription.value) > 0:
            for var in subscription.source.value.split(','):
                try:
                    k, v = var.split('=')
                except ValueError, e:
                    log.error(subscription.source.value)
                    log.error(var)
                    log.error(e)
                    continue
                params[k] = v

        self.url = '%s&%s' % (self.url, urlencode(params))
