# -*- coding: utf-8 -*-
import logging

from robot.worker.kickstarter.worker import KickstarterWorker

log = logging.getLogger(__name__)


class SingleItemKickstarterWorker(KickstarterWorker):
    def __init__(self, subscription):
        super(SingleItemKickstarterWorker, self).__init__(subscription)
