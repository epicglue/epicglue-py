# -*- coding: utf-8 -*-
import logging

from common.enums import ItemType
from robot.parser.instagram import Item
from robot.worker.instagram.worker import InstagramWorker

log = logging.getLogger(__name__)


class LocationInstagramWorker(InstagramWorker):
    data_key = 'data'
    parser = Item(item_type=ItemType.INSTAGRAM_LOCATION)

    expected_item_count = 100
    re_run_on_full_batch = True

    def __init__(self, subscription):
        super(LocationInstagramWorker, self).__init__(subscription)

        # Location ID should be stored as `value`
        self.url += '?access_token=%s&count=%d' % (self.get_token().token, self.expected_item_count)

    def get_errors(self, json):
        if 'data' not in json:
            return [json['meta']['error_message']]

        return []

    def get_next_url(self, json):
        """Feed endpoint has issues with min_id if id is out of scope, better to start always with frm beginning, adds overhead though"""
        return self.get_next_url_for_list(json)