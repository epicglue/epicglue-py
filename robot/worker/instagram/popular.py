# -*- coding: utf-8 -*-
import logging

from common.enums import ItemType
from robot.parser.instagram import Item
from robot.worker.instagram.worker import InstagramWorker

log = logging.getLogger(__name__)


class PopularInstagramWorker(InstagramWorker):
    data_key = 'data'
    parser = Item(item_type=ItemType.INSTAGRAM_POPULAR)
    content_order_field = 'created_at'

    def __init__(self, subscription):
        super(PopularInstagramWorker, self).__init__(subscription)

        self.url += '?access_token=%s' % self.get_token().token

    def get_errors(self, json):
        if 'data' not in json:
            return [json['meta']['error_message']]

        return []