# -*- coding: utf-8 -*-
import logging

from robot.parser.instagram import Item
from robot.worker.instagram.worker import InstagramWorker

log = logging.getLogger(__name__)


class FeedInstagramWorker(InstagramWorker):
    parser = Item()
    data_key = 'data'

    expected_item_count = 100
    re_run_on_full_batch = True
    use_state = False

    def __init__(self, subscription):
        super(FeedInstagramWorker, self).__init__(subscription)

        self.url += '?access_token=%s&count=%d' % (self.get_token().token, self.expected_item_count)

    def get_errors(self, json):
        if 'data' not in json:
            return [json['meta']['error_message']]

        return []