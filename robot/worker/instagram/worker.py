# -*- coding: utf-8 -*-
import logging
from urlparse import urlparse, urlunparse
from urlparse import parse_qs
from urllib import urlencode

from common.helpers import next_full_hour
from robot.worker.json_worker import JSONWorker

log = logging.getLogger(__name__)


class InstagramWorker(JSONWorker):
    def parse_response_headers(self, headers):
        self.sub.tokens_left = int(headers['x-ratelimit-remaining'])
        self.sub.tokens_used = int(headers['x-ratelimit-limit']) - self.sub.tokens_left
        self.sub.tokens_reset_at = next_full_hour()
        self.sub.save()

    def get_next_url_for_list(self, json):
        try:
            url = json['pagination']['next_url']
        except KeyError:
            url = self.url

        url_parts = urlparse(url)
        queries = parse_qs(url_parts.query)

        if 'max_id' in queries:
            del queries['max_id']

        if 'min_id' in queries:
            del queries['min_id']

        queries['min_id'] = json[self.data_key][0]['id']

        url = urlunparse(
            (url_parts[0], url_parts[1], url_parts[2], url_parts[3], urlencode(queries, True), url_parts[5]))

        return url