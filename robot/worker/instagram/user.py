# -*- coding: utf-8 -*-
import logging

from common.models import UserStat
from robot.parser.instagram import User
from robot.worker.instagram.worker import InstagramWorker


log = logging.getLogger(__name__)

class UserInstagramWorker(InstagramWorker):
    data_key = 'data'
    parser = User()

    def __init__(self, subscription):
        super(self.__class__, self).__init__(subscription)

        self.url += '?access_token=%s' % self.get_token().token

    def store(self, arr):
        del arr[0]['service']

        UserStat.objects.add_stats(self.sub.profile, arr[0])

        return []