from common.enums import MediaType, Visibility, ItemType
from robot.parser.parser import Parser


class Item(Parser):
    default_item_type = ItemType.PRODUCT_HUNT_POST

    def parse_item(self, json):
        obj = {
            'content_id': str(json['id']),
            'title': json['name'],
            'description': json['tagline'],
            'content_created_at': self.parse_time(json['created_at']),
            'item_type': self.item_type,
            'media_type': str(MediaType.LINK),
            'author': json['user']['username'],
            'author_image': json['user']['image_url']['32px'],
            'author_image_large': json['user']['image_url']['32px@3X'],
            'url': json['discussion_url'],
            'url_in': json['discussion_url'],
            'url_ext': json['redirect_url'],
            'visibility': str(Visibility.PUBLIC),
            'image': json['screenshot_url']['300px'],
            'image_small': json['screenshot_url']['300px'],
            'image_large': json['screenshot_url']['850px'],
            'image_width': 300,
            'image_height': 210,
            'image_large_width': 850,
            'image_large_height': 596,
            'image_small_width': 300,
            'image_small_height': 210,
            'comments': int(json['comments_count']),
            'points': int(json['votes_count'])
        }

        return obj
