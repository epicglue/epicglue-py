from common.enums import MediaType, Visibility, ItemType
from robot.parser.parser import Parser


class Item(Parser):
    def parse_item(self, json):
        vid_id = json['snippet']['thumbnails']['default']['url'].replace('https://i.ytimg.com/vi/', '').replace(
            '/default.jpg', '')

        obj = {
            'content_id': json['id'],
            'content_secondary_id': vid_id,
            'content_created_at': self.parse_time(json['snippet']['publishedAt']),
            'media_type': str(MediaType.VIDEO),
            'title': json['snippet']['title'],
            'description': json['snippet']['description'],
            'author': json['snippet']['channelTitle'],
            'url': 'http://www.youtube.com/watch?v=%s' % vid_id,
            'visibility': str(Visibility.PUBLIC),
            'image': json['snippet']['thumbnails']['default']['url'],
            'image_width': json['snippet']['thumbnails']['default']['width'],
            'image_height': json['snippet']['thumbnails']['default']['height'],
            'image_large': json['snippet']['thumbnails']['high']['url'],
            'image_large_width': json['snippet']['thumbnails']['high']['width'],
            'image_large_height': json['snippet']['thumbnails']['high']['height'],
        }

        if 'maxres' in json['snippet']['thumbnails']:
            obj['image_large'] = json['snippet']['thumbnails']['maxres']['url']
            obj['image_large_width'] = json['snippet']['thumbnails']['maxres']['width']
            obj['image_large_height'] = json['snippet']['thumbnails']['maxres']['height']

        snippet_type = json['snippet']['type']
        if snippet_type == 'upload':
            obj['item_type'] = str(ItemType.YOUTUBE_UPLOAD)
        elif snippet_type == 'like':
            obj['item_type'] = str(ItemType.YOUTUBE_LIKE)
        elif snippet_type == 'recommendation':
            obj['item_type'] = str(ItemType.YOUTUBE_RECOMMENDATION)
        elif snippet_type == 'bulletin':
            obj['item_type'] = str(ItemType.YOUTUBE_BULLETIN)
        elif snippet_type == 'playlistItem':
            obj['item_type'] = str(ItemType.YOUTUBE_PLAYLIST_ITEM)

        return obj