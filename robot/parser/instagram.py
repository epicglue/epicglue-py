from datetime import datetime

from pytz import utc

from common.enums import MediaType, Visibility, ItemType, StatType
from robot.parser.parser import Parser


class Item(Parser):
    default_item_type = ItemType.INSTAGRAM_POST

    def parse_time(self, str):
        return datetime.fromtimestamp(float(str), tz=utc)

    def parse_item(self, json):
        if json['type'] == 'image':
            media_type = str(MediaType.PHOTO)
        else:
            media_type = str(MediaType.VIDEO)

        obj = {
            'content_id': json['id'],
            'content_created_at': self.parse_time(json['created_time']),
            'item_type': self.item_type,
            'media_type': media_type,
            'author': json['user']['username'],
            'author_image': json['user']['profile_picture'],
            'url': json['link'],
            'url_in': json['link'],
            'tags': set(json['tags']),
            'visibility': str(Visibility.PRIVATE),
            'image': json['images']['low_resolution']['url'],
            'image_large': json['images']['standard_resolution']['url'],
            'image_small': json['images']['thumbnail']['url'],
            'image_width': json['images']['low_resolution']['width'],
            'image_height': json['images']['low_resolution']['height'],
            'image_large_width': json['images']['standard_resolution']['width'],
            'image_large_height': json['images']['standard_resolution']['height'],
            'image_small_width': json['images']['thumbnail']['width'],
            'image_small_height': json['images']['thumbnail']['height'],
            'comments': json['comments']['count'],
            'points': json['likes']['count']
        }

        if 'videos' in json:
            obj['video'] = json['videos']['low_resolution']['url']
            obj['video_width'] = json['videos']['low_resolution']['width']
            obj['video_height'] = json['videos']['low_resolution']['height']

            obj['video_large'] = json['videos']['standard_resolution']['url']
            obj['video_large_width'] = json['videos']['standard_resolution']['width']
            obj['video_large_height'] = json['videos']['standard_resolution']['height']

        if 'caption' in json and json['caption'] is not None and 'text' in json['caption']:
            obj['title'] = json['caption']['text']

        if json['location'] is not None and 'latitude' in json['location'] and 'longitude' in json['location']:
            obj['location'] = [json['location']['latitude'], json['location']['longitude']]

        if json['location'] is not None and 'name' in json['location']:
            obj['location_name'] = json['location']['name']

        return obj


class User(Parser):
    def parse_item(self, json):
        return {
            str(StatType.FOLLOWERS): int(json['counts']['followed_by']),
            str(StatType.FOLLOWING): int(json['counts']['follows']),
            str(StatType.POSTS): int(json['counts']['media'])
        }
