# -*- coding: utf-8 -*-
import HTMLParser
from datetime import datetime

from django.utils.html import strip_tags
from pytz import utc

from common.enums import MediaType, Visibility, ItemType, StatType
from robot.parser.parser import Parser

html = HTMLParser.HTMLParser()


class HackerNewsParser(Parser):
    default_item_type = ItemType.HACKER_NEWS_STORY


class AlgoliaItem(HackerNewsParser):
    def parse_time(self, str):
        return datetime.fromtimestamp(float(str), tz=utc)

    def parse_item(self, json):
        obj = {
            'content_id': json['objectID'],
            'content_created_at': self.parse_time(json['created_at_i']),
            'item_type': str(ItemType.HACKER_NEWS_STORY),
            'media_type': str(MediaType.LINK),
            'title': json['title'],
            'author': json['author'],
            'url': 'https://news.ycombinator.com/item?id=%s' % json['objectID'],
            'url_in': 'https://news.ycombinator.com/item?id=%s' % json['objectID'],
            'url_ext': json['url'] if json['url'] is not None and len(json['url']) > 0 else None,
            'visibility': str(Visibility.PUBLIC),
            'points': int(json['points']),
            'comments': int(json['num_comments'])
        }

        if obj['title'].startswith('Ask HN:'):
            obj['item_type'] = str(ItemType.HACKER_NEWS_ASK)
        elif obj['title'].startswith('Show HN:'):
            obj['item_type'] = str(ItemType.HACKER_NEWS_SHOW)

        if json['story_text'] is not None and len(json['story_text']):
            obj['description'] = strip_tags(html.unescape(json['story_text']))

        return obj


class HackerNewsItem(HackerNewsParser):
    def parse_time(self, str):
        return datetime.fromtimestamp(float(str), tz=utc)

    def parse_item(self, json):
        obj = {
            'content_id': str(json['id']),
            'content_created_at': self.parse_time(json['time']),
            'item_type': str(ItemType.HACKER_NEWS_STORY),
            'media_type': str(MediaType.LINK),
            'title': json['title'],
            'author': json['by'],
            # 'url': json['url'] if 'url' in json and len(json['url']) > 0 else 'https://news.ycombinator.com/item?id=%s' % str(json['id']),
            'url': 'https://news.ycombinator.com/item?id=%s' % str(json['id']),
            'url_in': 'https://news.ycombinator.com/item?id=%s' % str(json['id']),
            'url_ext': json['url'] if 'url' in json and len(json['url']) > 0 else None,
            'visibility': str(Visibility.PUBLIC),
            'points': json['score'],
        }

        if obj['title'].startswith('Ask HN:'):
            obj['item_type'] = str(ItemType.HACKER_NEWS_ASK)
        elif obj['title'].startswith('Show HN:'):
            obj['item_type'] = str(ItemType.HACKER_NEWS_SHOW)
        elif json['type'] == 'job':
            obj['item_type'] = str(ItemType.HACKER_NEWS_JOB)

        if 'text' in json and len(json['text']):
            obj['description'] = strip_tags(html.unescape(json['text']))

        return obj


class User(HackerNewsParser):
    def parse_item(self, json):
        return {
            str(StatType.KARMA): int(json['karma']),
            str(StatType.POSTS): int(json['submission_count']),
            str(StatType.COMMENTS): int(json['comment_count'])
        }
