from datetime import datetime

from pytz import utc

from common.enums import MediaType, ItemType, Visibility
from common.helpers import now
from robot.parser.parser import Parser


class Item(Parser):
    default_item_type = ItemType.KICKSTARTER_FUND

    def parse_time(self, str):
        return datetime.fromtimestamp(float(str), tz=utc)

    def parse_item(self, json):
        # goal: 1000,
        # pledged: 222,
        # state: "live",
        # country: "US",
        # currency: "USD",
        # currency_symbol: "$",
        # currency_trailing_code: true,
        # deadline: 1425766766,
        # backers_count: 15,

        return {
            'content_id': str(json['id']),
            'title': json['name'],
            'description': self.build_description(json),
            'author': json['creator']['name'],
            'content_created_at': self.parse_time(json['launched_at']),
            'content_updated_at': self.parse_time(json['state_changed_at']),
            'url': json['urls']['web']['project'],
            'url_in': json['urls']['web']['project'],
            'image_large': json['photo']['full'],
            'image_small': json['photo']['small'],
            'image': json['photo']['med'],
            'image_large_width': 560,
            'image_large_height': 420,
            'image_small_width': 160,
            'image_small_height': 120,
            'image_width': 266,
            'image_height': 200,
            'author_image': json['creator']['avatar']['thumb'],
            'author_image_large': json['creator']['avatar']['medium'],
            'location_name': json['location']['displayable_name'],
            'tags': [json['category']['name']],
            'media_type': str(MediaType.AUCTION),
            'item_type': self.item_type,
            'visibility': str(Visibility.PUBLIC)
        }

    def build_description(self, json):
        goal = '%s%s' % (json['currency_symbol'], json['goal'])
        if json['currency_trailing_code']:
            goal = '%s %s' % (json['goal'], json['currency_symbol'])

        pledged = '%s%s' % (json['currency_symbol'], json['pledged'])
        if json['currency_trailing_code']:
            pledged = '%s %s' % (json['pledged'], json['currency_symbol'])

        return '\n'.join([
            json['blurb'],
            '',
            '%d backers' % json['backers_count'],
            'Funded %d%%' % int((float(json['pledged']) / float(json['goal'])) * 100),
            'Pledged %s of %s goal' % (pledged, goal),
            '%d days to go' % (self.parse_time(json['deadline']) - now()).days
        ])
