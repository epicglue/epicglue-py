import HTMLParser
import time
from datetime import datetime

from pytz import utc

from common.enums import MediaType, ItemType, Visibility, StatType
from robot.parser.parser import Parser

html = HTMLParser.HTMLParser()

class Tweet(Parser):
    default_item_type = ItemType.TWITTER_TWEET

    def parse_time(self, str):
        return datetime.fromtimestamp(time.mktime(time.strptime(str, '%a %b %d %H:%M:%S +0000 %Y')), tz=utc)

    def parse_item(self, json):
        obj = {
            'content_id': str(json['id']),
            'content_created_at': self.parse_time(json['created_at']),
            'media_type': str(MediaType.TWEET),
            'title': html.unescape(json['text']),
            'author': json['user']['screen_name'],
            'author_image': json['user']['profile_image_url'],
            'url': 'https://twitter.com/%s/status/%s' % (json['user']['screen_name'], json['id']),
            'url_in': 'https://twitter.com/%s/status/%s' % (json['user']['screen_name'], json['id']),
            'comments': int(json['retweet_count']),
            'points': int(json.get('favourites_count', 0))
        }

        if 'retweeted' in json and json['retweeted']:
            obj['item_type'] = str(ItemType.TWITTER_RETWEET)
        else:
            obj['item_type'] = self.item_type

        if len(json['entities']['hashtags']) > 0:
            tags = []
            for tag in json['entities']['hashtags']:
                tags.append(tag['text'])
            obj['tags'] = set(tags)

        if 'media' in json['entities'] and len(json['entities']['media']) > 0:
            media = json['entities']['media'][0]

            obj['image'] = media['media_url'] + ':small'
            obj['image_width'] = media['sizes']['small']['w']
            obj['image_height'] = media['sizes']['small']['h']
            obj['image_small'] = media['media_url'] + ':thumb'
            obj['image_small_width'] = media['sizes']['thumb']['w']
            obj['image_small_height'] = media['sizes']['thumb']['h']
            obj['image_large'] = media['media_url'] + ':medium'
            obj['image_large_width'] = media['sizes']['medium']['w']
            obj['image_large_height'] = media['sizes']['medium']['h']

        if not json['user']['protected']:
            obj['visibility'] = str(Visibility.PUBLIC)
        else:
            obj['visibility'] = str(Visibility.PRIVATE)

        if 'favorite_count' in json:
            obj['points'] = int(json['favorite_count'])

        if 'coordinates' in json and json['coordinates'] is not None and 'coordinates' in json['coordinates']:
            obj['location'] = json['coordinates']['coordinates']

        return obj


class User(Parser):
    def parse_item(self, json):
        return {
            str(StatType.FOLLOWERS): int(json['followers_count']),
            str(StatType.FOLLOWING): int(json['friends_count']),
            str(StatType.POSTS): int(json['statuses_count']),
            str(StatType.LISTED): int(json['listed_count']),
            str(StatType.LIKED): int(json['favourites_count'])
        }
