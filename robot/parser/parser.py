from dateutil.parser import parse

from common.enums import ItemType


class Parser(object):
    default_item_type = ItemType.DEFAULT

    def __init__(self, item_type=None):
        if item_type is None: item_type = self.default_item_type

        self.item_type = str(item_type)

    def parse_time(self, str):
        return parse(str)

    def parse_item(self, json):
        return {}
