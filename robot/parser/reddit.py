import HTMLParser
from datetime import datetime
import re

from django.utils.html import strip_tags
from pytz import utc

from common.enums import ItemType, MediaType, Visibility, StatType
from robot.parser.parser import Parser

html = HTMLParser.HTMLParser()

class RedditParser(Parser):
    def parse_time(self, str):
        return datetime.fromtimestamp(float(str), tz=utc)


class T1(RedditParser):
    """
    Comment

    "subreddit_id": "t5_2s7cz",
    "edited": false,
    "banned_by": null,
    "link_id": "t3_1r7nud",
    "link_author": "Eagles1000",
    "likes": true,
    "replies": null,
    "user_reports": [],
    "saved": false,
    "id": "cdld5vs",
    "gilded": 0,
    "archived": true,
    "report_reasons": null,
    "author": "yezooz",
    "parent_id": "t3_1r7nud",
    "score": 1,
    "approved_by": null,
    "controversiality": 0,
    "body": "https://www.youtube.com/watch?v=hbfb-n9jH1w&amp;list=PLo4ndFVF1ojhaCKh32EVsSr8CYNxzbHjo",
    "link_title": "Anybody here attending the London Fall Club Tour tonight ?",
    "author_flair_css_class": null,
    "downs": 0,
    "body_html": "&lt;div class=\"md\"&gt;&lt;p&gt;&lt;a hr...lt;/p&gt;\n&lt;/div&gt;",
    "subreddit": "skrillex",
    "score_hidden": false,
    "name": "t1_cdld5vs",
    "created": 1385239464.0,
    "author_flair_text": null,
    "link_url": "http://www.reddit.com/r/skrillex/comments/1r7nud/anybody_here_attending_the_london_fall_club_tour/",
    "created_utc": 1385239464.0,
    "distinguished": null,
    "mod_reports": [],
    "num_reports": null,
    "ups": 1
    """
    default_item_type = ItemType.REDDIT_COMMENT

    def parse_item(self, json):
        return {
            'title': json['link_title'],
            'description': json['body'],
            'author': json['link_author'],
            'url': json['url'],
            'content_id': json['name'],
            'content_created_at': self.parse_time(str(json['created_utc'])),
            'image': json['thumbnail'],
            'points': int(json['score'])
        }


class T2(RedditParser):
    """
    Account

    "has_mail": false,
    "name": "yezooz",
    "is_friend": false,
    "created": 1240017198.0,
    "hide_from_robots": false,
    "gold_creddits": 0,
    "modhash": "hrf8k4b1kyec5bf12cc8b5f959f108ec332cbaf75f64b36a97",
    "created_utc": 1240013598.0,
    "link_karma": 153,
    "comment_karma": 2,
    "over_18": true,
    "is_gold": false,
    "is_mod": false,
    "gold_expiration": null,
    "inbox_count": 0,
    "has_verified_email": false,
    "id": "3g7zb",
    "has_mod_mail": false
    """

    def parse_item(self, json):
        return {
            str(StatType.KARMA): int(json['link_karma']),
            str(StatType.COMMENT_KARMA): int(json['comment_karma']),
        }


class T3(RedditParser):
    """
    Link

    "domain": "i.imgur.com",
    "banned_by": null,
    "media_embed": {},
    "subreddit": "london",
    "selftext_html": null,
    "selftext": "",
    "likes": false,
    "user_reports": [],
    "secure_media": null,
    "link_flair_text": null,
    "id": "1xlmsx",
    "gilded": 0,
    "archived": true,
    "clicked": false,
    "report_reasons": null,
    "author": "Lolworth",
    "num_comments": 61,
    "score": 211,
    "approved_by": null,
    "over_18": false,
    "hidden": false,
    "thumbnail": "http://e.thumbs.redditmedia.com/PY9yiRvdx2-_V4Q4.jpg",
    "subreddit_id": "t5_2qkog",
    "edited": false,
    "link_flair_css_class": null,
    "author_flair_css_class": null,
    "downs": 0,
    "secure_media_embed": {},
    "saved": false,
    "stickied": false,
    "is_self": false,
    "permalink": "/r/london/comments/1xlmsx/metro_readers_hit_back/",
    "name": "t3_1xlmsx",
    "created": 1392109932.0,
    "url": "http://i.imgur.com/s65P4JP.png",
    "author_flair_text": null,
    "title": "Metro readers hit back",
    "created_utc": 1392109932.0,
    "distinguished": null,
    "media": null,
    "mod_reports": [],
    "visited": false,
    "num_reports": null,
    "ups": 211
    """
    default_item_type = str(ItemType.REDDIT_LINK)

    image_providers = ['imgur.com', 'i.imgur.com']
    youtube_providers = ['youtube.com', 'youtu.be']

    imgur = re.compile('http(s?)://imgur.com/([A-Za-z0-9]+)$')
    i_imgur = re.compile('http(s?)://i.imgur.com/([A-Za-z0-9]+).jpg$')

    def parse_item(self, json):
        json = json['data']

        obj = {
            'title': json['title'],
            'author': json['author'],
            'content_id': json['name'],
            'content_created_at': self.parse_time(str(int(json['created_utc']))),
            'points': int(json['score']),
            'comments': int(json['num_comments']),
            'url': 'https://www.reddit.com%s' % json['permalink'],
            'url_in': 'https://www.reddit.com%s' % json['permalink'],
            'url_ext': json['url'],
            'tags': [json['subreddit'], '/r/%s' % json['subreddit']],
            'item_type': self.default_item_type,
            'visibility': str(Visibility.PUBLIC)
        }

        if 'selftext' in json:
            obj['description'] = json['selftext']

        if self.imgur.match(json['url']) is not None or \
                        self.i_imgur.match(json['url']) is not None:
            obj['media_type'] = str(MediaType.PHOTO)
            if self.i_imgur.match(json['url']) is not None:
                img = self.parse_i_imgur_path(json['url'])
            else:
                img = self.parse_imgur_path(json['url'])

            obj['image'] = img['medium']
            obj['image_height'] = img['medium_height']
            obj['image_width'] = img['medium_width']
            obj['image_small'] = img['small']
            obj['image_small_height'] = img['small_height']
            obj['image_small_width'] = img['small_width']
            obj['image_large'] = img['large']
            obj['image_large_height'] = img['large_height']
            obj['image_large_width'] = img['large_width']
        elif 'media' in json and \
                        json['media'] is not None and \
                        'type' in json['media'] and \
                        json['media']['type'] in self.youtube_providers:
            obj['media_type'] = str(MediaType.VIDEO)

            if 'oembed' in json['media'] and 'url' in json['media']['oembed']:
                vid_id = json['media']['oembed']['url'].replace('http://www.youtube.com/watch?v=', '')
                obj['image'] = 'http://img.youtube.com/vi/%s/hqdefault.jpg' % vid_id
                obj['image_width'] = 480
                obj['image_height'] = 360

                obj['image_small'] = 'http://img.youtube.com/vi/%s/mqdefault.jpg' % vid_id
                obj['image_small_width'] = 320
                obj['image_small_height'] = 180

                obj['image_large'] = 'http://img.youtube.com/vi/%s/sddefault.jpg' % vid_id
                obj['image_large_width'] = 640
                obj['image_large_height'] = 480
                obj['video'] = json['url']
                obj['video_height'] = json['media']['oembed']['height']
                obj['video_width'] = json['media']['oembed']['width']
        else:
            obj['media_type'] = str(MediaType.COMMENT)

        if 'title' in obj:
            obj['title'] = strip_tags(html.unescape(obj['title']))
        if 'description' in obj:
            obj['description'] = strip_tags(html.unescape(obj['description']))

        return obj

    def parse_i_imgur_path(self, path):
        return {
            'medium': path.replace('.jpg', 'm.jpg'),
            'medium_width': 320,
            'medium_height': 320,
            'large': path.replace('.jpg', 'l.jpg'),
            'large_width': 640,
            'large_height': 640,
            'small': path.replace('.jpg', 's.jpg'),
            'small_width': 160,
            'small_height': 160,
        }

    def parse_imgur_path(self, path):
        return self.parse_i_imgur_path(path.replace('http://', 'http://i.') + '.jpg')


class T4(RedditParser):
    """
    Message

    "body": "barcelona",
    "was_comment": false,
    "first_message": null,
    "name": "t4_ibx9j",
    "first_message_name": null,
    "created": 1352212560.0,
    "dest": "soccerbot",
    "author": "yezooz",
    "created_utc": 1352212560.0,
    "body_html": "&lt;!-- SC_OFF --&gt;&lt;div class=\"md\"&gt;&lt;p&gt;barcelona&lt;/p&gt;\n&lt;/div&gt;&lt;!-- SC_ON --&gt;",
    "subreddit": null,
    "parent_id": null,
    "context": "",
    "replies": "",
    "id": "ibx9j",
    "new": false,
    "distinguished": null,
    "subject": "crest"
    """
    default_item_type = ItemType.REDDIT_MESSAGE

    def parse_item(self, json):
        return {
            'title': json['subject'],
            'description': json['body'],
            'author': json['author'],
            'content_id': json['name'],
            'content_created_at': self.parse_time(str(json['created_utc']))
        }


class T5(RedditParser):
    """
    Subreddit

    "banner_img": "",
    "submit_text_html": null,
    "user_is_banned": false,
    "id": "2qh33",
    "user_is_contributor": false,
    "submit_text": "",
    "display_name": "funny",
    "header_img": "http://a.thumbs.redditmedia.com/VPKTvfChLglNonYrL8EmkJWGN1MOPY8Sd8NXMFuyhJ8.png",
    "description_html": "&lt;!-- SC_OFF --&gt;&lt;div class=\"md\"&gt;&lt;p&gt;&lt;strong&gt;Welcome to r/Funny:&lt;/strong&gt;&lt;/p&gt;\n\n&lt;p&gt;You may only post if you are funny. &lt;/p&gt;\n\n&lt;hr/&gt;\n\n&lt;p&gt;&lt;a href=\"/wiki/reddit_101\"&gt;New to reddit? Click here!&lt;/a&gt;&lt;/p&gt;\n\n&lt;hr/&gt;\n\n&lt;h1&gt;Rules&lt;/h1&gt;\...&gt;&lt;!-- SC_ON --&gt;",
    "title": "funny",
    "collapse_deleted_comments": false,
    "over18": false,
    "public_description_html": null,
    "icon_size": null,
    "icon_img": "",
    "header_title": "OK WE GET IT; YOU HATE SANDWICHES",
    "description": "**Welcome to r/Funny:**\n\nYou may only post if you are funny. \n\n-----\n\n[New to reddit? Click here!](/wiki/reddit_... for anything else",
    "submit_link_label": "Submit a humorous link",
    "accounts_active": null,
    "public_traffic": true,
    "header_size": [
        160,
        64
    ],
    "subscribers": 7941302,
    "submit_text_label": "Submit a comical text post",
    "name": "t5_2qh33",
    "created": 1201242956.0,
    "url": "/r/funny/",
    "created_utc": 1201242956.0,
    "banner_size": null,
    "user_is_moderator": false,
    "public_description": "",
    "comment_score_hide_mins": 0,
    "subreddit_type": "public",
    "submission_type": "any",
    "user_is_subscriber": true
    """
    default_item_type = ItemType.REDDIT_SUB

    def parse_item(self, json):
        return {
            'title': json['header_title'],
            'description': json['description'],
            'author': json['display_name'],
            'url': json['url'],
            'content_id': json['name'],
            'content_created_at': self.parse_time(str(json['created_utc'])),
            'image': json['header_img'],
            'points': int(json['score'])
        }


class T6(RedditParser):
    """
    Award

    icon_70: "https://s3.amazonaws.com/redditstatic/award/reddit_gold-70.png",
    description: "Since August 2015",
    url: "/gold/about",
    icon_40: "https://s3.amazonaws.com/redditstatic/award/reddit_gold-40.png",
    award_id: "v",
    id: "xoawv",
    name: "reddit gold"
    """
    default_item_type = ItemType.REDDIT_AWARD

    def parse_item(self, json):
        return {
            'title': json['name'],
            'description': json['description'],
            'author': json['display_name'],
            'url': json['url'],
            'content_id': json['id'],
            'content_created_at': datetime.utcnow(),
        }


class T8(RedditParser):
    """Promo Campaign"""
    pass
