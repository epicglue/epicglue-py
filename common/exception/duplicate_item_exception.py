# -*- coding: utf-8 -*-
class DuplicateItemException(Exception):
    def __init__(self, msg="Duplicate Item"):
        super(DuplicateItemException, self).__init__(msg)