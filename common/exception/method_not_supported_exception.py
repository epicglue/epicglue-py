# -*- coding: utf-8 -*-
class MethodNotSupportedException(Exception):
    def __init__(self, msg='Method Not Supported'):
        super(MethodNotSupportedException, self).__init__(msg)