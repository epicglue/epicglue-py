# -*- coding: utf-8 -*-
class NoTokensLeftException(Exception):
    def __init__(self, msg="No Tokens Left"):
        super(NoTokensLeftException, self).__init__(msg)