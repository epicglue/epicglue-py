# -*- coding: utf-8 -*-
class NotImplementedException(Exception):
    def __init__(self):
        super(NotImplementedException, self).__init__('Method not implemented')