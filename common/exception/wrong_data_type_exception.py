# -*- coding: utf-8 -*-
class WrongDataTypeException(Exception):
    def __init__(self):
        super(WrongDataTypeException, self).__init__('Wrong data type')