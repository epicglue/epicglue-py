# -*- coding: utf-8 -*-
import logging
import os

log = logging.getLogger(__name__)


def store_raw_json(root_dir, item_hash, content):
    n = 2
    hash_parts = [item_hash[i:i + n] for i in range(0, len(item_hash), n)]

    path = os.path.join(root_dir, *hash_parts[:-1])

    try:
        if not os.path.exists(path):
            os.makedirs(path)

        file_path = os.path.join(path, '%s.json' % hash_parts[-1])

        handler = open(file_path, 'w')

        handler.write(content)

        handler.close()
    except IOError, e:
        log.warning('Failed to save JSON -- %s' % file_path)
        raise e