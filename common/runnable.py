# -*- coding: utf-8 -*-
import logging

log = logging.getLogger(__name__)


class Runnable(object):
    def run(self):
        pass
