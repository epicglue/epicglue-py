# -*- coding: utf-8 -*-
import logging
from random import randint
import sys
from traceback import format_tb
import time

from django.conf import settings
from elasticsearch import Elasticsearch, ConflictError, ConnectionTimeout, RequestError

from common.enums import ConflictPolicy, EpicEnum
from common.helpers import now

log = logging.getLogger(__name__)


class DocType(EpicEnum):
    DEFAULT = 'item'


class ES(object):
    def __init__(self):
        self.instance = Elasticsearch()

    @staticmethod
    def get_stat_doc_type():
        return now().strftime('logs_%Y_%m_%d')

    @staticmethod
    def get_index_name(index):
        if settings.TESTING or settings.CONFIG_NAME == 'test':
            return '%s_temp' % index
        elif settings.CONFIG_NAME == 'dev':
            return '%s_temp' % index
        return index

    # INSERT / UPDATE

    def index(self, index, doc_type, insert_doc, update_doc=None, doc_id=None, conflict_policy=ConflictPolicy.IGNORE):
        index = self.get_index_name(index)

        try:
            if not self.document_exists(index, doc_id):
                self.instance.create(index=index, doc_type=doc_type, id=doc_id, body=insert_doc)
            else:
                self.instance.update(index=index, doc_type=doc_type, id=doc_id, body={'doc': update_doc})
        except ConflictError, ex:
            if conflict_policy == ConflictPolicy.REPLACE:
                attempts = 3
                for i in xrange(0, attempts):
                    try:
                        time.sleep(randint(1, 100) / 100.0)
                        self.instance.update(index=index, doc_type=doc_type, id=doc_id, body={'doc': update_doc})
                    except ConflictError, ex:
                        if i == attempts - 1:
                            raise ex
            elif conflict_policy == ConflictPolicy.FAIL:
                log.warning('ES conflict, failed')
                raise ex
        except ConnectionTimeout:
            log.warning('ES timed out')
            ex_type, ex, tb = sys.exc_info()
            log.error(format_tb(tb))
            raise ex

    def add_to_list(self, index, item_id, key, value):
        self.instance.update(
            index=index,
            doc_type=str(DocType.DEFAULT),
            id=item_id,
            body={
                'script_file': 'add_to_list',
                'lang': 'groovy',
                'params': {
                    'list': key,
                    'value': value
                }
            }
        )

    def increment_stat_value(self, index, item_id, key, value=1):
        index = self.get_index_name(index)

        if isinstance(value, basestring):
            raise Exception('Incrementing not supported for strings')

        try:
            self.instance.update(
                index=index,
                doc_type=self.get_stat_doc_type(),
                id=item_id,
                body={
                    'script_file': 'increment_value',
                    'lang': 'groovy',
                    'params': {
                        'key': key,
                        'value': value
                    },
                    'upsert': {
                        key: value
                    }
                }
            )
        except RequestError:
            self.instance.update(
                index=index,
                doc_type=self.get_stat_doc_type(),
                id=item_id,
                body={
                    'script_file': 'increment_value',
                    'lang': 'groovy',
                    'params': {
                        'key': key,
                        'value': value
                    }
                }
            )

    def replace_stat_value(self, index, item_id, key, value):
        index = self.get_index_name(index)

        if isinstance(value, basestring):
            self.instance.update(
                index=index,
                doc_type=self.get_stat_doc_type(),
                id=item_id,
                body={
                    'script_file': 'replace_value',
                    'lang': 'groovy',
                    'params': {
                        'key': key,
                        'value': value
                    },
                    'upsert': {
                        key: value
                    }
                },

            )
        else:
            self.instance.update(
                index=index,
                doc_type=self.get_stat_doc_type(),
                id=item_id,
                body={
                    'script_file': 'replace_value',
                    'lang': 'groovy',
                    'params': {
                        'key': key,
                        'value': value
                    },
                    'upsert': {
                        key: value
                    }
                }
            )

    # SELECT

    def count(self, index):
        pass

    def get_value(self, index, key, doc_type=None, item_key=None):
        if doc_type is None:
            doc_type = self.get_stat_doc_type()

        index_item = self.instance.get(index=self.get_index_name(index), doc_type=doc_type, id=key)

        if item_key is None:
            return index_item
        else:
            return index_item['_source'].get(item_key)

    def find(self, index, query=None):
        if query is None:
            return self.find_all(index)

        return self.instance.search(index=self.get_index_name(index), body=query)

    def find_all(self, index=None):
        if index is not None:
            index = self.get_index_name(index)

        return self.instance.search(index=index, body={'query': {'match_all': {}}})

    # MISC

    def document_exists(self, index, hash):
        return self.instance.exists(index, hash)

    def index_exists(self, index):
        return self.instance.indices.exists(index=self.get_index_name(index))

    def refresh_index(self, index):
        if self.index_exists(index):
            self.instance.indices.refresh(index=self.get_index_name(index))
            log.debug('%s index refreshed' % self.get_index_name(index))

    def flush(self, index=None):
        """Hope you are what what you are doing?!"""
        if index is None:
            self.instance.indices.flush()
            log.debug('Entire ES flushed')
        elif self.index_exists(index):
            self.instance.indices.flush(index=self.get_index_name(index))
            log.debug('%s index flushed' % self.get_index_name(index))

    def delete(self, index):
        if self.index_exists(index):
            self.instance.indices.delete(index=self.get_index_name(index))
            log.debug('%s index deleted' % self.get_index_name(index))
