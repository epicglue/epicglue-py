# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta
import random
import string
import re

from pytz import utc

log = logging.getLogger(__name__)


def now(tz=utc):
    return datetime.now(tz=tz)


def next_full_hour():
    r = now() + timedelta(hours=1)

    return datetime(r.year, r.month, r.day, r.hour, 0)


def random_string(length=16):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))


def random_number(length=16):
    return ''.join(random.choice(string.digits) for i in range(length))


def convert_to_camel_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def convert_to_underscore(word):
    return ''.join(x.capitalize() or '_' for x in word.split('_'))


def s_to_qs(value):
    return int(float(value) * 1000000000.0)


def s_to_string(value):
    value = float(value)

    if value * 1000000.0 < 1000:
        return "%.3fqs" % (value * 1000000.0)
    if value * 1000.0 < 1000:
        return "%.3fms" % (value * 1000.0)

    return "%.3fs" % value
