from django.contrib import admin

from models import Plan


class PlanAdmin(admin.ModelAdmin):
    list_filter = ['is_visible']
    list_display = ['name', 'product_id', 'ttl']


admin.site.register(Plan, PlanAdmin)
