# -*- coding: utf-8 -*-
from enum import Enum


class EpicEnum(Enum):
    def __str__(self):
        if isinstance(self.value, tuple):
            return self.value[0]
        return self.value


class ConflictPolicy(EpicEnum):
    REPLACE = 'replace'
    IGNORE = 'ignore'
    FAIL = 'fail'


class MediaType(EpicEnum):
    LINK = 'link'
    PHOTO = 'photo'
    VIDEO = 'video'
    TEXT = 'text'
    EVENT = 'event'
    TWEET = 'tweet'
    PAYMENT = 'payment'
    LIKE = 'like'
    COMMENT = 'comment'
    AUCTION = 'auction'


class ItemType(EpicEnum):
    DEFAULT = 'default'

    INSTAGRAM = 'instagram'
    INSTAGRAM_POST = 'instagram.post'
    INSTAGRAM_POPULAR = 'instagram.popular'
    INSTAGRAM_LOCATION = 'instagram.location'
    INSTAGRAM_LIKE = 'instagram.like'
    INSTAGRAM_COMMENT = 'instagram.comment'
    INSTAGRAM_FOLLOWING = 'instagram.following'
    INSTAGRAM_FOLLOWER = 'instagram.follower'

    TWITTER = 'twitter'
    TWITTER_TWEET = 'twitter.tweet'
    TWITTER_RETWEET = 'twitter.retweet'
    TWITTER_DM = 'twitter.dm'
    # TWITTER_BLOCK = 'twitter.block'
    # TWITTER_LIST = 'twitter.list'
    # TWITTER_LIST_MEMBER = 'twitter.list.member'
    # TWITTER_LIST_SUBSCRIBER = 'twitter.list.subscriber'

    FACEBOOK = 'facebook'
    FACEBOOK_FEED = 'facebook.feed'
    FACEBOOK_PHOTO = 'facebook.photo'
    FACEBOOK_PHOTO_COMMENT = 'facebook.photo.comment'
    FACEBOOK_PHOTO_LIKE = 'facebook.photo.like'
    FACEBOOK_PHOTO_TAG = 'facebook.photo.tag'
    FACEBOOK_VIDEO = 'facebook.video'
    FACEBOOK_VIDEO_COMMENT = 'facebook.video.comment'
    FACEBOOK_VIDEO_LIKE = 'facebook.video.like'
    FACEBOOK_VIDEO_TAG = 'facebook.video.tag'
    # FACEBOOK_PHOTO_ALBUM = 'facebook.photo_album'
    # FACEBOOK_ACTIVITY = 'facebook.activity'
    # FACEBOOK_BOOK = 'facebook.book'
    # FACEBOOK_EVENT = 'facebook.event'
    # FACEBOOK_FAMILY_MEMBER = 'facebook.family_member'
    FACEBOOK_FRIEND = 'facebook.friend'
    # FACEBOOK_FRIEND_LIST = 'facebook.friend_list'
    # FACEBOOK_GROUP = 'facebook.group'
    # FACEBOOK_INTEREST = 'facebook.interest'
    # FACEBOOK_LIKED = 'facebook.liked'
    # FACEBOOK_MOVIE = 'facebook.movie'
    # FACEBOOK_MUSIC = 'facebook.music'
    # FACEBOOK_POKE = 'facebook.poke'
    # FACEBOOK_TV = 'facebook.tv'
    FACEBOOK_MESSAGE = 'facebook.dm'

    HACKER_NEWS = 'hn'
    HACKER_NEWS_TOP_STORY = 'hn.top_story'
    HACKER_NEWS_STORY = 'hn.story'
    HACKER_NEWS_ASK = 'hn.ask'
    HACKER_NEWS_SHOW = 'hn.show'
    HACKER_NEWS_JOB = 'hn.job'

    PRODUCT_HUNT = 'product_hunt'
    PRODUCT_HUNT_POST = 'product_hunt.post'

    YOUTUBE = 'yt'
    YOUTUBE_UPLOAD = 'yt.upload'
    YOUTUBE_LIKE = 'yt.like'
    YOUTUBE_RECOMMENDATION = 'yt.recommendation'
    YOUTUBE_BULLETIN = 'yt.bulletin'
    YOUTUBE_PLAYLIST_ITEM = 'yt.playlistItem'

    KICKSTARTER = 'kickstarter'
    KICKSTARTER_FUND = 'kickstarter.fund'

    REDDIT = 'reddit'
    REDDIT_POST = 'reddit.post'
    REDDIT_LINK = 'reddit.link'
    REDDIT_SUB = 'reddit.sub'
    REDDIT_COMMENT = 'reddit.comment'
    REDDIT_MESSAGE = 'reddit.message'
    REDDIT_AWARD = 'reddit.award'
    REDDIT_PROMO = 'reddit.promo'


class Service(EpicEnum):
    INSTAGRAM = 'instagram'
    TWITTER = 'twitter'
    FACEBOOK = 'facebook'
    HACKER_NEWS = 'hn'
    YOUTUBE = 'yt'
    REDDIT = 'reddit'
    KICKSTARTER = 'kickstarter'
    PRODUCT_HUNT = 'product_hunt'
    GOOGLE = 'google'
    FLICKR = 'flickr'
    FIVEHUNDREDPX = '500px'
    BITLY = 'bitly'
    DELICIOUS = 'delicious'


class StatType(EpicEnum):
    KARMA = 'karma'
    COMMENT_KARMA = 'comment_karma'
    FOLLOWING = 'following'
    FOLLOWERS = 'followers'
    POSTS = 'posts'
    COMMENTS = 'comments'
    LIKED = 'liked'
    LISTED = 'listed'


class SubscriptionStatus(EpicEnum):
    READY = 'ready'
    IN_PROGRESS = 'progress'
    ENQUEUED = 'enqueued'
    FAILED = 'failed'


class Visibility(EpicEnum):
    PUBLIC = 'public'
    PRIVATE = 'private'