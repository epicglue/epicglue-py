from datetime import datetime

from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from common.helpers import now
from robot.models import Service, UserServiceProfile


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=255, null=True, blank=True, default=None)
    salt = models.CharField(max_length=255, null=True, blank=True, default=None)
    device_udid_list = ArrayField(models.CharField(max_length=64), null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'user'

    def __unicode__(self):
        return u'Profile of user: %s' % self.user.username


class UserSocial(models.Model):
    user = models.ForeignKey(User, db_index=True)
    service = models.ForeignKey(Service, db_index=True)
    json = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'user_social'

    def __unicode__(self):
        return u'UserSocial (%s, %s)' % (str(self.user), str(self.service))


class UserStatManager(models.Manager):
    def add_stat(self, profile, key, value, timestamp=None):
        if timestamp is None:
            ts = now()
        else:
            ts = timestamp

        us, created = self.get_or_create(profile=profile, date=datetime(ts.year, ts.month, ts.day), key=key, defaults={'value': value})

        if not created:
            us.value = value
            us.save()

    def add_stats(self, profile, value_dict, timestamp=None):
        for k,v in value_dict.iteritems():
            self.add_stat(profile, k, v, timestamp)

    def get_latest_stat(self, profile, key):
        return self.filter(profile=profile, key=key).values('value').order_by('-date')[0]

    def get_latest_value(self, profile, key):
        return self.get_latest_stat(profile, key)['value']


class UserStat(models.Model):
    user = models.ForeignKey(User, db_index=True, null=True, blank=True, default=None)
    profile = models.ForeignKey(UserServiceProfile, db_index=True)
    date = models.DateField()
    key = models.CharField(max_length=30)
    value = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    objects = UserStatManager()

    class Meta:
        db_table = 'user_stat'

    def __str__(self):
        return '%s stats (%d)' % (self.profile, self.value)


class UserLogManager(models.Manager):
    pass


class UserLog(models.Model):
    user = models.ForeignKey(User, db_index=True, null=True, blank=True, default=None)
    date = models.DateTimeField()
    key = models.CharField(max_length=255, null=True, blank=True, default=None)
    log = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    objects = UserLogManager()

    class Meta:
        db_table = 'user_log'

    def __str__(self):
        return '%s (%s)' % (self.log, self.user)

    @staticmethod
    def add_log(log, user=None, key=None, date=now()):
        UserLog.objects.create(log=log, user=user, key=key, date=date)


class UserTokenManager(models.Manager):
    pass


class UserToken(models.Model):
    user = models.ForeignKey(User, db_index=True, null=True, blank=True, default=None)
    token = models.CharField(max_length=255)
    expiry = models.DateTimeField(null=True, blank=True, default=None)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    objects = UserTokenManager()

    class Meta:
        db_table = 'user_token'


class PlanManager(models.Manager):
    pass


class Plan(models.Model):
    name = models.CharField(max_length=100)
    short_name = models.SlugField(max_length=30, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True, default=None)
    product_id = models.SlugField(max_length=30, unique=True)
    ttl = models.IntegerField()
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2)
    is_visible = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    objects = PlanManager()

    class Meta:
        db_table = 'plan'

    def __str__(self):
        return 'Plan %s (%s)' % (self.name, self.product_id)


class UserPaymentManager(models.Manager):
    pass


class UserPayment(models.Model):
    user = models.ForeignKey(User, db_index=True)
    product_id = models.CharField(max_length=255)
    payment_id = models.CharField(max_length=255)
    original_payment_id = models.CharField(max_length=255)
    amount = models.IntegerField()
    net_amount = models.IntegerField()
    payment_time = models.DateTimeField(null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    objects = UserPaymentManager()

    class Meta:
        db_table = 'user_payment'

    def __str__(self):
        return 'user_%d paid for %s worth %d on %s' % (self.user.id, self.product_id, self.amount, str(self.payment_time))


class UserMetricManager(models.Manager):
    pass


class UserMetric(models.Model):
    user = models.ForeignKey(User, db_index=True, null=True, blank=True, default=None)
    key = models.CharField(max_length=200)
    value = models.DecimalField(max_digits=16, decimal_places=4)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    objects = UserMetricManager()

    class Meta:
        db_table = 'user_metric'
        unique_together = [['user', 'key']]
