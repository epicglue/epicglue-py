from common.enums import Service

SERVICES = {
    str(Service.FACEBOOK): {
        'clientID': '229493077245396',
        'clientSecret': '06eea14625fab1e1244bdea94c71b287',
        'callbackURL': 'http://10.0.0.2:3000/auth/facebook/callback',
        'enableProof': False
    },
    str(Service.TWITTER): {
        'clientID': 'eVgB2VmLwKZnGLZ4BgEH0mZta',
        'clientSecret': 'rE84LmfT3hRemy9PTaMHP1zJyAtKYrno64xpuSBhmCBvnEjzNM',
        'callbackURL': 'http://10.0.0.2:3000/auth/twitter/callback'
    },
    str(Service.INSTAGRAM): {
        'clientID': '9fb281a3fc0b4fb383c1767e0623f671',
        'clientSecret': '269931ada8484bb39a51f961abb34ad9',
        'callbackURL': 'http://10.0.0.2:8000/service/instagram/callback',
        'scopes': [
            'basic',  # 'comments',  # 'relationships',  # 'likes'
        ]
    },
    str(Service.PRODUCT_HUNT): {
        'clientID': 'f15e94a90b7f7566ebffded66a8cdc9bdfb8fcbe65050b66a5a7994dc65c0521',
        'clientSecret': '5523ca2c34593330b2a24f9f3c3208841cd206268346d6eeee224b158ef69d4f',
        'callbackURL': 'http://localhost:8000/service/product_hunt/callback'
    },
    str(Service.GOOGLE): {
        'clientID': '10938504778.apps.googleusercontent.com',
        'APIkey': 'AIzaSyAKe8qWq9-4WwLz-b50IoWlccaOZN1JmhM',
        'scopes': [
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/plus.circles.read',
            'https://www.googleapis.com/auth/plus.stream.read',
            'https://www.googleapis.com/auth/calendar.readonly',
            'https://picasaweb.google.com/data/',
            'https://www.googleapis.com/auth/youtube.readonly',
            'https://www.googleapis.com/auth/tasks.readonly'
        ]
    },
    str(Service.YOUTUBE): {
        'scopes': [
            'https://www.googleapis.com/auth/youtube.readonly'
        ]
    },
    str(Service.FLICKR): {
        'clientID': 'f8f6f8cdfaf4fdebaa6f9673156ec3a9',
        'clientSecret': '8ad51feb374709c4'
    },
    str(Service.FIVEHUNDREDPX): {
        'clientID': 'TrtysGPS2eVwYhbFva6pRRXgLvAzqkCzOuHbT2PC',
        'clientSecret': '0uwt2D0bNvkgNRt9c0wyTlQB7xOlg0cgsN6sTPi7',
        'callbackURL': 'http://10.0.0.2:3000/auth/500px/callback'
    },
    str(Service.BITLY): {
        'clientID': 'ac4abba6f68445799c7ee6097c907d1d978d8b0b',
        'clientSecret': '14dcdbb437e524bcf81bb2e99409d3d0722ea977',
        'callbackURL': 'http://10.0.0.2:3000/auth/bitly/callback'
    },
    str(Service.DELICIOUS): {
        'clientID': 'c5eed266cfe241c1fc341da5b2e8f73d',
        'clientSecret': 'f43c9ce0bf17bcd58af1d10b26744979'
    },
    str(Service.REDDIT): {

    }
}



# SERVICES = {
#     str(Service.FACEBOOK): {
#         'clientID': '1409207309353920',
#         'clientSecret': '89ebeed9d61117d593bdf4aa66bbd3d9',
#         'callbackURL': 'http://www.epicglue.com/auth/facebook/callback',
#         'enableProof': False
#     },
#     str(Service.TWITTER): {
#         'clientID': 'YaPufDcj2x1XE8AcRCYJnIWNh',
#         'clientSecret': 'lmzmU35hA6XwDdwkZjdFT5PazmSiT0YscPJB5s2D1B2eZP6yaC',
#         'callbackURL': 'http://www.epicglue.com/auth/twitter/callback'
#     },
#     str(Service.INSTAGRAM): {
#         'clientID': '65a2e4a5c36648e190e94ac530f8ac85',
#         'clientSecret': '7f3e274b557b4f9bb6983f477a52bff6',
#         'callbackURL': 'http://www.epicglue.com/auth/instagram/callback'
#     },
#     str(Service.FLICKR): {
#         'clientID': '87298cdeacd9f7c0080b7d1c5d930df3',
#         'clientSecret': '6cc55c8d0cc85454'
#     },
#     str(Service.FIVEHUNDREDPX): {
#         'clientID': 'MmqbFgi32V8YR4b5Y61WMTpTlaKJlOiMYaufNDyn',
#         'clientSecret': 'QeJD2XcEdc2lsEFqm4hY3TlCplDjc9cq7wR9k5Ea',
#         'callbackURL': 'http://www.epicglue.com/auth/500px/callback'
#     },
#     str(Service.BITLY): {
#         'clientID': 'd2675f44e0412bd258ebe4e9599fef6005e6bf34',
#         'clientSecret': '34132e66a89f743b5176aa1b412ec73b43cf647f',
#         'callbackURL': 'http://www.epicglue.com/auth/bitly/callback'
#     },
#     str(Service.DELICIOUS): {
#         'clientID': '968f5147c75842fe632f867e545d5826',
#         'clientSecret': '54bdb7fbad53893d5ca69f4ea50d376d'
#     },
#     str(Service.YOUTUBE): {
#         'scopes': [
#             'https://www.googleapis.com/auth/youtube.readonly'
#         ]
#     },
#     str(Service.REDDIT): {
#
#     }
# }
