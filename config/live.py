try:
    from epicglue.settings import *
    from config.api.live import *
except ImportError, e:
    pass

DEBUG = False

CONFIG_NAME = 'live'
TESTING = False
ALLOWED_HOSTS = ('127.0.0.1', '46.4.68.112', 'epic1', 'admin.epicglue.com', 'epicglue.com')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': 'epic_test',
        'PASSWORD': 'epic',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379',
        'OPTIONS': {
            'DB': 0,
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            }
        },
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'logfile': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/epicglue/py_error.log',
            'maxBytes': 5000000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['logfile'],
            'propagate': True,
            'level': 'ERROR'
        },
        'api': {
            'handlers': ['logfile'],
            'propagate': True,
            'level': 'ERROR'
        },
        'test': {
            'handlers': ['logfile'],
            'propagate': True,
            'level': 'ERROR'
        },
        'robot': {
            'handlers': ['logfile'],
            'propagate': True,
            'level': 'ERROR'
        }
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'default'

BROKER_URL = 'redis://localhost:6379/1'
BROKER_TRANSPORT_OPTIONS = {
    'visibility_timeout': 60
}

# CELERY_TASK_PUBLISH_RETRY_POLICY = {
#     'max_retries': 3,
#     'interval_start': 0,
#     'interval_step': 0.2,
#     'interval_max': 0.2,
# }
CELERY_ENABLE_UTC = True
CELERY_ALWAYS_EAGER = False
CELERY_IGNORE_RESULT = True
CELERY_MESSAGE_COMPRESSION = 'gzip'