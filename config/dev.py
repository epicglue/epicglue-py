import sys

try:
    from epicglue.settings import *
    from config.api.dev import *
except ImportError, e:
    print e

DEBUG = True

CONFIG_NAME = 'dev'
TESTING = sys.argv[1:2] == ['test']
# ALLOWED_HOSTS = ('127.0.0.1', '46.4.68.112', 'epic1')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'epic_glue_dev',
        'PORT': '5433',
        'HOST': '127.0.0.1',
        'USER': 'epic',
        'PASSWORD': 'epic'
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379',
        'OPTIONS': {
            'DB': 1,
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            }
        },
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/epicglue/py_info.log',
            'formatter': 'standard',
        },
        'logfile_error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/epicglue/py_error.log',
            'formatter': 'standard',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'logstash': {
            'level': 'DEBUG',
            'class': 'logstash.LogstashHandler',
            'host': 'localhost',
            'port': 5959,
            'version': 1,
            'message_type': 'logstash',
            'fqdn': False,
            'tags': ['django'],
        },
    },
    'loggers': {
        'root': {
            'handlers': ['logstash', 'logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'DEBUG'
        },
        'django': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'INFO'
        },
        'api': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'DEBUG'
        },
        'test': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'DEBUG'
        },
        'robot': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'DEBUG'
        }
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'default'

CELERY_ENABLE_UTC = True
CELERY_ALWAYS_EAGER = True
CELERY_IGNORE_RESULT = True

# CELERY_TASK_PUBLISH_RETRY_POLICY = {
#     'max_retries': 3,
#     'interval_start': 0,
#     'interval_step': 0.2,
#     'interval_max': 0.2,
# }

TOKENS = {
    'hipchat': 'ee34a94ded0436958b378400b7f552',
}