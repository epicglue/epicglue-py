import sys

try:
    from epicglue.settings import *
    from config.api.test import *
except ImportError, e:
    pass

DEBUG = True

CONFIG_NAME = 'test'
TESTING = sys.argv[1:2] == ['test']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'epicglue_test',
        'HOST': '127.0.0.1',
        'USER': 'epic_test',
        'PASSWORD': 'epic',
        'PORT': '5432'
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379',
        'OPTIONS': {
            'DB': 3,
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            }
        },
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'logfile': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/epicglue/py_info.log',
            'maxBytes': 5000000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'logfile_error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/epicglue/py_error.log',
            'maxBytes': 5000000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'INFO'
        },
        'api': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'INFO'
        },
        'test': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'INFO'
        },
        'robot': {
            'handlers': ['logfile', 'logfile_error', 'console'],
            'propagate': True,
            'level': 'INFO'
        }
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'default'

BROKER_URL = 'redis://localhost:6379/4'
BROKER_TRANSPORT_OPTIONS = {
    'visibility_timeout': 60
}

BROKER_BACKEND = 'memory'
CELERY_ALWAYS_EAGER = True

TOKENS = {
    'hipchat': 'ee34a94ded0436958b378400b7f552',
}

AWS_ACCESS_KEY_ID = 'AKIAJAFOZD3JWMWBN6AA'
AWS_SECRET_ACCESS_KEY = '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'